use rapier2d::{
	prelude::*,
	data::ComponentSetMut,
};

/// Trait used by [RigidBodyProperties](crate::dynamics::RigidBodyProperties) and
/// [ColliderProperties](crate::geometry::ColliderProperties)
/// to access components in storage. Blanket-implemented for implementations of [R2dNode].
pub trait SetComponent<T> {
	fn set_component(&mut self, value: T);
}

impl<T: MapStorageMut<C, ()>, C: Send + Sync + 'static> SetComponent<C> for T {
	#[inline]
	fn set_component(&mut self, value: C) {
		self.map_storage_mut(|c| *c = value)
	}
}

/// Trait to be implemented by the user for custom nodes that "inherit" properties from
/// [R2dRigidBody](crate::default_nodes::R2dRigidBody),
/// [R2dCollider](crate::default_nodes::R2dCollider),
/// or [R2dJoint](crate::default_nodes::R2dJoint).
///
/// [RigidBodyProperties](crate::dynamics::RigidBodyProperties) and
/// [ColliderProperties](crate::geometry::ColliderProperties) are generically implemented in terms
/// of `R2dNode + AsRef<[...]Components> + AsMut<[...]Components>`.
pub trait R2dNode<Components> {
	fn components(&self) -> &Components;
	
	fn components_mut(&mut self) -> &mut Components;
	
	/// Override this method to add a prefix to all properties registered by `register_r2d_properties`
	#[inline]
	fn property_prefix() -> String {
		"".to_string()
	}
}

pub trait MapStorageMut<C, Ret> {
	/// The main method required to implement
	/// [RigidBodyProperties](crate::geometry::RigidBodyProperties)/[ColliderProperties](crate::geometry::ColliderProperties).
	fn map_storage_mut(&mut self, f: impl for<'c> FnOnce(&'c mut C) -> Ret + Send + Sync + 'static) -> ();
}

/// Trait alias for [SetComponent] for all RigidBody components.
pub trait SetRigidBodyComponents:
  SetComponent<RigidBodyType>
+ SetComponent<RigidBodyPosition>
+ SetComponent<RigidBodyVelocity>
+ SetComponent<RigidBodyMassProps>
+ SetComponent<RigidBodyForces>
+ SetComponent<RigidBodyActivation>
+ SetComponent<RigidBodyDamping>
+ SetComponent<RigidBodyDominance>
+ SetComponent<RigidBodyCcd>
+ SetComponent<RigidBodyChanges>
+ SetComponent<RigidBodyIds>
+ SetComponent<RigidBodyColliders> {
	/// Convenience method for `<Self as SetComponent<C>>::set_component`.
	#[inline]
	fn set<C>(&mut self, value: C)
	where Self: SetComponent<C> {
		<Self as SetComponent<C>>::set_component(self, value)
	}
}

impl<T> SetRigidBodyComponents for T
where T:
  SetComponent<RigidBodyType>
+ SetComponent<RigidBodyPosition>
+ SetComponent<RigidBodyVelocity>
+ SetComponent<RigidBodyMassProps>
+ SetComponent<RigidBodyForces>
+ SetComponent<RigidBodyActivation>
+ SetComponent<RigidBodyDamping>
+ SetComponent<RigidBodyDominance>
+ SetComponent<RigidBodyCcd>
+ SetComponent<RigidBodyChanges>
+ SetComponent<RigidBodyIds>
+ SetComponent<RigidBodyColliders> {}

/// Trait alias for [SetComponent] for all Collider components.
pub trait SetColliderComponents:
  SetComponent<ColliderType>
+ SetComponent<ColliderShape>
+ SetComponent<ColliderPosition>
+ SetComponent<ColliderMaterial>
+ SetComponent<ColliderFlags>
+ SetComponent<ColliderMassProps>
+ SetComponent<ColliderChanges>
+ SetComponent<ColliderBroadPhaseData> {
	/// Convenience method for `<Self as SetComponent<C>>::set_component`.
	#[inline]
	fn set<C>(&mut self, value: C)
	where Self: SetComponent<C> {
		<Self as SetComponent<C>>::set_component(self, value)
	}
}

impl<T> SetColliderComponents for T
where T:
  SetComponent<ColliderType>
+ SetComponent<ColliderShape>
+ SetComponent<ColliderPosition>
+ SetComponent<ColliderMaterial>
+ SetComponent<ColliderFlags>
+ SetComponent<ColliderMassProps>
+ SetComponent<ColliderChanges>
+ SetComponent<ColliderBroadPhaseData> {}
