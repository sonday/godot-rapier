macro_rules! maybe_custom_default {
	() => { Default::default() };
	($def:expr) => { $def };
}

macro_rules! components {
	($(#[$attr:meta])* struct $T:ty { $($component:ident : $t:ty $(= $def:expr)? ,)* }) => {
		::paste::paste! {
			$(#[$attr])*
			pub struct [<$T Components>] {
				$($component: $t,)*
			}
			
			/// Trait generated to auto-update values in storage for custom [R2dNodes](crate::R2dNode).
			pub trait [<$T ComponentAccessors>]: R2dNode<[<$T Components>]>{
				$(
					fn $component(&self) -> &$t {
						&self.components().$component
					}
					
					fn [<set_ $component>](&mut self, value: $t)
					where Self: SetComponent<$t> {
						self.[<apply_to_ $component>](|t| *t = value.clone());
					}
					
					fn [<apply_to_ $component>]<Ret>(&mut self, f: impl Fn(&mut $t) -> Ret) -> Ret
					where Self: SetComponent<$t> {
						let ret = f(&mut self.components_mut().$component);
						self.set_component(self.components().$component.clone());
						ret
					}
				)*
			}
			
			impl [<$T Components>] {
				pub fn new() -> Self {
					Self {
						$($component : maybe_custom_default!($($def)?),)*
					}
				}
			}
			
			impl Default for [<$T Components>] {
				fn default() -> Self {
					Self::new()
				}
			}
		}
	};
}



macro_rules! properties {
	(|$T:ty| $($prop:ident : $gd_ty:ty => $(($deref:tt))? $component:ident $(. $field:ident)* ;)*) => {
		::paste::paste! {
			impl [<$T Components>] {
				$(
					/// For easier setting of defaults in register_properties
					#[inline]
					pub(crate) fn $prop(&self) -> $gd_ty {
						self.$component$(.$field)*.to_gd()
					}
				)*
			}
			
			/// Trait for registering and accessing properties on custom [R2dNodes](crate::R2dNode).
			pub trait [<$T Properties>]: [<$T ComponentAccessors>] + [<Set $T Components>] {
				/// Add `#[register_with(Self::register_r2d_properties)]` to your `NativeClass` declaration,
				/// or call from another method passed to that attribute to add properties to your custom node.
				fn register_r2d_properties(builder: &ClassBuilder<Self>) where Self: Sized;
				
				/// Call from an exported `_set` method to override setting the owner node's position/rotation
				/// to also set the positition/rotation in Rapier.
				fn override_set_position_and_rotation(&mut self, owner: &[<$T Base>], property: GodotString, value: Variant) -> bool;
				
				$(
					#[inline]
					fn $prop(&self) -> $gd_ty {
						self.components().$prop()
					}
					
					#[inline]
					fn [<set_ $prop>](&mut self, value: $gd_ty) {
						self.[<apply_to_ $component>](|val| $($deref)?val$(.$field)* = value.to_rapier());
					}
				)*
			}
		}
	};
}
