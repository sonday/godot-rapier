use std::{
	convert::TryFrom,
	sync::atomic::Ordering::Relaxed
};

use atomig::{Atom, Atomic};
use once_cell::sync::Lazy;
use rapier2d::{
	math::{Isometry, Real, Rotation, Translation, Vector, },
	na::UnitComplex,
	prelude::*
};

use gdnative::{
	prelude::*,
	nativescript::{Export, Map, },
	api::Resource,
};

use crate::{geometry::material::R2dColliderMaterial, geometry::shapes::*,};

pub trait GetIsometry {
	fn local_translation(&self) -> Translation<Real>;
	fn local_rotation(&self) -> Rotation<Real>;
	#[inline(always)]
	fn local_isometry(&self) -> Isometry<Real> {
		Isometry::from_parts(self.local_translation(), self.local_rotation())
	}
	fn global_translation(&self) -> Translation<Real>;
	fn global_rotation(&self) -> Rotation<Real>;
	#[inline(always)]
	fn global_isometry(&self) -> Isometry<Real> {
		Isometry::from_parts(self.global_translation(), self.global_rotation())
	}
}

impl<T: SubClass<Node2D>> GetIsometry for T {
	#[inline]
	fn local_translation(&self) -> Translation<Real> {
		let pos = self.upcast().position();
		Translation::new(pos.x as Real, pos.y as Real)
	}
	
	#[inline]
	fn local_rotation(&self) -> Rotation<Real> {
		let rot = self.upcast().rotation();
		Rotation::new(rot as Real)
	}
	
	#[inline]
	fn global_translation(&self) -> Translation<Real> {
		let pos = self.upcast().global_position();
		Translation::new(pos.x as Real, pos.y as Real)
	}
	
	#[inline]
	fn global_rotation(&self) -> Rotation<Real> {
		let rot = self.upcast().global_rotation();
		Rotation::new(rot as Real)
	}
}

pub trait CloneFromRapier<R> {
	fn clone_from_rapier(input: &R) -> Self;
}

pub trait CloneFromGd<G> {
	fn clone_from_gd(input: &G) -> Self;
}

pub trait ToRapier<R> {
	fn to_rapier(&self) -> R;
}

pub trait ToGd<G> {
	fn to_gd(&self) -> G;
}

impl<R: CloneFromGd<G>, G> ToRapier<R> for G {
	#[inline]
	fn to_rapier(&self) -> R {
		R::clone_from_gd(self)
	}
}

impl<G: CloneFromRapier<R>, R> ToGd<G> for R {
	#[inline]
	fn to_gd(&self) -> G {
		G::clone_from_rapier(self)
	}
}

impl CloneFromRapier<Vector<Real>> for Vector2 {
	#[inline]
	fn clone_from_rapier(input: &Vector<Real>) -> Self {
		Vector2::new(input.x as f32, input.y as f32)
	}
}

impl CloneFromGd<Vector2> for Vector<Real> {
	#[inline]
	fn clone_from_gd(input: &Vector2) -> Self {
		Vector::new(input.x as Real, input.y as Real)
	}
}

impl CloneFromGd<Real> for Real {
	#[inline]
	fn clone_from_gd(&input: &Real) -> Self {
		input
	}
}

impl CloneFromRapier<Real> for Real {
	#[inline]
	fn clone_from_rapier(&input: &Real) -> Self {
		input
	}
}

impl CloneFromRapier<RigidBodyType> for u64 {
	#[inline]
	fn clone_from_rapier(&input: &RigidBodyType) -> Self {
		input as u64
	}
}

impl CloneFromGd<u64> for RigidBodyType {
	#[inline]
	fn clone_from_gd(&input: &u64) -> Self {
		use RigidBodyType::*;
		const D: u64 = Dynamic as u64;
		const S: u64 = Static as u64;
		const KP: u64 = KinematicPositionBased as u64;
		const KV: u64 = KinematicVelocityBased as u64;
		match input {
			D => Dynamic,
			S => Static,
			KP => KinematicPositionBased,
			KV => KinematicVelocityBased,
			e => panic!("Unknown RigidBodyType: {}", e),
		}
	}
}

impl CloneFromGd<Vector2> for Translation<Real> {
	#[inline]
	fn clone_from_gd(input: &Vector2) -> Self {
		Translation::new(input.x as Real, input.y as Real)
	}
}

impl CloneFromRapier<Translation<Real>> for Vector2 {
	#[inline]
	fn clone_from_rapier(input: &Translation<Real>) -> Self {
		Vector2::new(input.x as f32, input.y as f32)
	}
}

impl CloneFromGd<Real> for UnitComplex<Real> {
	#[inline]
	fn clone_from_gd(&input: &Real) -> Self {
		Self::new(input)
	}
}

impl CloneFromRapier<UnitComplex<Real>> for Real {
	#[inline]
	fn clone_from_rapier(input: &UnitComplex<Real>) -> Self {
		input.angle()
	}
}

impl CloneFromRapier<bool> for bool {
	#[inline]
	fn clone_from_rapier(&input: &bool) -> Self {
		input
	}
}

impl CloneFromGd<bool> for bool {
	#[inline]
	fn clone_from_gd(&input: &bool) -> Self {
		input
	}
}

impl CloneFromRapier<RigidBodyDominance> for i8 {
	#[inline]
	fn clone_from_rapier(&input: &RigidBodyDominance) -> Self {
		input.0
	}
}

impl CloneFromGd<i8> for RigidBodyDominance {
	#[inline]
	fn clone_from_gd(&input: &i8) -> Self {
		RigidBodyDominance(input)
	}
}

impl CloneFromRapier<ColliderType> for u64 {
	#[inline]
	fn clone_from_rapier(&input: &ColliderType) -> Self {
		input as u64
	}
}

impl CloneFromGd<u64> for ColliderType {
	#[inline]
	fn clone_from_gd(&input: &u64) -> Self {
		use ColliderType::*;
		const SOLID: u64 = Solid as u64;
		const SENSOR: u64 = Sensor as u64;
		match input {
			SOLID => Solid,
			SENSOR => Sensor,
			e => panic!("unknown ColliderType: {}", e)
		}
	}
}

impl CloneFromRapier<ActiveHooks> for u32 {
	fn clone_from_rapier(input: &ActiveHooks) -> Self {
		input.bits()
	}
}

impl CloneFromGd<u32> for ActiveHooks {
	fn clone_from_gd(&input: &u32) -> Self {
		Self::from_bits(input).expect(&*format!("unknown ActiveHooks flags: {:b}", input))
	}
}

impl CloneFromGd<ShapeInstance> for ColliderShape {
	fn clone_from_gd(input: &ShapeInstance) -> Self {
		input.shape.shared_shape()
	}
}

impl CloneFromRapier<ColliderMaterial> for Instance<R2dColliderMaterial, Shared> {
	fn clone_from_rapier(input: &ColliderMaterial) -> Self {
		Instance::emplace(
			R2dColliderMaterial {
				friction: Atomic::new(input.friction),
				restitution: Atomic::new(input.restitution),
				friction_combine_rule: Atomic::new(input.friction_combine_rule as u8),
				restitution_combine_rule: Atomic::new(input.restitution_combine_rule as u8),
			}
		).into_shared()
	}
}

impl CloneFromGd<Option<Instance<R2dColliderMaterial, Shared>>> for ColliderMaterial {
	fn clone_from_gd(input: &Option<Instance<R2dColliderMaterial, Shared>>) -> Self {
		input.as_ref().map_or(ColliderMaterial::default(), |inst| inst.script().map(|mat|
			ColliderMaterial {
				friction: mat.friction.load(Relaxed),
				restitution: mat.restitution.load(Relaxed),
				friction_combine_rule: mat.friction_combine_rule.load(Relaxed).to_rapier(),
				restitution_combine_rule: mat.restitution_combine_rule.load(Relaxed).to_rapier(),
			}
		).unwrap())
	}
}

impl CloneFromGd<u8> for CoefficientCombineRule {
	fn clone_from_gd(&input: &u8) -> Self {
		use CoefficientCombineRule::*;
		const AVG: u8 = Average as u8;
		const MIN: u8 = Min as u8;
		const MUL: u8 = Multiply as u8;
		const MAX: u8 = Max as u8;
		match input {
			AVG => Average,
			MIN => Min,
			MUL => Multiply,
			MAX => Max,
			e => panic!("unknown CoefficientCombineRule: {}", e)
		}
	}
}

impl CloneFromRapier<CoefficientCombineRule> for u8 {
	fn clone_from_rapier(&input: &CoefficientCombineRule) -> Self {
		input as Self
	}
}

impl CloneFromRapier<Point<Real>> for Vector2 {
	fn clone_from_rapier(input: &Point<Real>) -> Self {
		Self::new(input.x as f32, input.y as f32)
	}
}

impl CloneFromGd<Vector2> for Point<Real> {
	fn clone_from_gd(input: &Vector2) -> Self {
		Self::new(input.x as Real, input.y as Real)
	}
}

impl CloneFromRapier<SharedShape> for Ref<Resource, Unique> {
	fn clone_from_rapier(input: &SharedShape) -> Self {
		ShapeInstance::<Unique>::try_from(input)
			.expect("couldn't make ShapeInstance from SharedShape")
			.owner
	}
}

impl CloneFromRapier<SharedShape> for Ref<Resource, Shared> {
	fn clone_from_rapier(input: &SharedShape) -> Self {
		Ref::<Resource, Unique>::clone_from_rapier(input).into_shared()
	}
}

impl CloneFromGd<Ref<Resource, Shared>> for SharedShape {
	fn clone_from_gd(input: &Ref<Resource>) -> Self {
		unsafe { ShapeInstance::try_from(input.assume_safe()) }
			.expect("failed to cast Resource to R2dShape")
			.shape.shared_shape()
	}
}

impl CloneFromRapier<u32> for u32 {
	fn clone_from_rapier(&input: &u32) -> Self {
		input
	}
}

impl CloneFromGd<u32> for u32 {
	fn clone_from_gd(&input: &u32) -> Self {
		input
	}
}

impl CloneFromRapier<ActiveEvents> for u32 {
	fn clone_from_rapier(input: &ActiveEvents) -> Self {
		input.bits()
	}
}

impl CloneFromGd<u32> for ActiveEvents {
	fn clone_from_gd(&input: &u32) -> Self {
		Self::from_bits(input).expect(&*format!("unknown ActiveEvents flags: {:b}", input))
	}
}


/// Trait to convert to & from `atomig::Atom` types for external types that don't implement it.
/// Used especially by shapes and JointParams for [AtomicProperties](AtomicProperty)
pub trait AsAtom {
	type Atom: Atom;
	
	fn into_atom(self) -> Self::Atom;
	
	fn from_atom(atom: Self::Atom) -> Self;
}

impl AsAtom for Vector2 {
	type Atom = u64;
	
	fn into_atom(self) -> Self::Atom {
		unsafe { std::mem::transmute(self) }
	}
	
	fn from_atom(atom: Self::Atom) -> Self {
		unsafe { std::mem::transmute(atom) }
	}
}

impl AsAtom for Real {
	type Atom = Self;
	
	fn into_atom(self) -> Self::Atom {
		self
	}
	
	fn from_atom(atom: Self::Atom) -> Self {
		atom
	}
}

/// Wrapper struct for simplifying defining atomic properties to allow for
/// [NativeClasses](::gdnative::prelude::NativeClass) with
/// [UserData = ArcData<Self>](::gdnative::prelude::ArcData).
pub struct AtomicProperty<T: AsAtom>(pub Atomic<T::Atom>);

impl<T: AsAtom> AtomicProperty<T> {
	pub fn new(value: T) -> Self {
		Self(Atomic::new(value.into_atom()))
	}
	
	/// Calls [load](::atomig::Atomic::load)([Relaxed](::atomig::Ordering::Relaxed)).
	pub fn load(&self) -> T {
		T::from_atom(self.0.load(Relaxed))
	}
	
	/// Calls [store](::atomig::Atomic::load)(value, [Relaxed](::atomig::Ordering::Relaxed)).
	pub fn store(&self, value: T) {
		self.0.store(value.into_atom(), Relaxed)
	}
}

impl<T: ToVariant + AsAtom> ToVariant for AtomicProperty<T> {
	fn to_variant(&self) -> Variant {
		self.load().to_variant()
	}
}

impl<T: FromVariant + AsAtom> FromVariant for AtomicProperty<T> {
	fn from_variant(variant: &Variant) -> Result<Self, FromVariantError> {
		Ok(Self::new(T::from_variant(variant)?))
	}
}

impl<T: AsAtom + Export> Export for AtomicProperty<T> {
	type Hint = <T as Export>::Hint;
	
	fn export_info(hint: Option<Self::Hint>) -> ExportInfo {
		<T as Export>::export_info(hint)
	}
}

impl<T: Default + AsAtom> Default for AtomicProperty<T> {
	fn default() -> Self {
		Self::new(T::default())
	}
}

pub(crate) static GLOBAL_POS_NAME: Lazy<GodotString> = Lazy::new(|| GodotString::from("global_position"));
pub(crate) static GLOBAL_ROT_NAME: Lazy<GodotString> = Lazy::new(|| GodotString::from("global_rotation"));
pub(crate) static LOCAL_POS_NAME: Lazy<GodotString> = Lazy::new(|| GodotString::from("position"));
pub(crate) static LOCAL_ROT_NAME: Lazy<GodotString> = Lazy::new(|| GodotString::from("rotation"));
