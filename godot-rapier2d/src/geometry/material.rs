use atomig::Atomic;
use rapier2d::{
	prelude::*,
	dynamics::CoefficientCombineRule::{self, *},
};
use gdnative::{
	prelude::*,
	api::Resource,
	nativescript::property::{IntHint, EnumHint}
};
use std::sync::atomic::Ordering::Relaxed;

#[derive(NativeClass, Debug,)]
#[inherit(Resource)]
#[register_with(Self::_bind_methods)]
#[user_data(ArcData<Self>)]
pub struct R2dColliderMaterial {
	/// The friction coefficient of the collider to be built.
	pub friction: Atomic<Real>,
	/// The rule used to combine two friction coefficients.
	pub friction_combine_rule: Atomic<u8>,
	/// The restitution coefficient of the collider to be built.
	pub restitution: Atomic<Real>,
	/// The rule used to combine two restitution coefficients.
	pub restitution_combine_rule: Atomic<u8>,
}

#[methods]
impl R2dColliderMaterial {
	pub fn new(_owner: &Resource) -> Self {
		Self {
			friction: Atomic::new(1.0),
			restitution: Atomic::new(0.0),
			friction_combine_rule: Atomic::new(0),
			restitution_combine_rule: Atomic::new(0),
		}
	}
	
	fn _bind_methods(builder: &ClassBuilder<Self>) {
		builder.add_property::<Real>("friction")
			.with_default(ColliderMaterial::default().friction)
			.with_getter(|this: &Self, _| this.friction.load(Relaxed))
			.with_shr_setter(|this: &Self, _, value| this.friction.store(value, Relaxed))
			.done();
		
		builder.add_property::<Real>("restitution")
			.with_default(ColliderMaterial::default().restitution)
			.with_getter(|this: &Self, _| this.restitution.load(Relaxed))
			.with_shr_setter(|this: &Self, _, value| this.restitution.store(value, Relaxed))
			.done();
		
		builder.add_property::<u8>("friction_combine_rule")
			.with_hint(IntHint::Enum(EnumHint::new(vec![
				"Average".into(), "Min".into(), "Multiply".into(), "Max".into(),
			])))
			.with_default(Average as u8)
			.with_getter(|this: &Self, _| this.friction_combine_rule.load(Relaxed))
			.with_shr_setter(|this: &Self, _, value|
				this.friction_combine_rule.store(value, Relaxed))
			.done();
		
		builder.add_property::<u8>("restitution_combine_rule")
			.with_hint(IntHint::Enum(EnumHint::new(vec![
				"Average".into(), "Min".into(), "Multiply".into(), "Max".into(),
			])))
			.with_default(Average as u8)
			.with_getter(|this: &Self, _| this.restitution_combine_rule.load(Relaxed))
			.with_shr_setter(|this: &Self, _, value|
				this.restitution_combine_rule.store(value, Relaxed))
			.done();
	}
	
	pub fn component(&self) -> ColliderMaterial {
		let friction_combine_rule = coeff_rule_from_u8(self.friction_combine_rule.load(Relaxed));
		let restitution_combine_rule = coeff_rule_from_u8(self.restitution_combine_rule.load(Relaxed));
		
		ColliderMaterial {
			friction: self.friction.load(Relaxed),
			restitution: self.restitution.load(Relaxed),
			friction_combine_rule,
			restitution_combine_rule,
		}
	}
}

#[inline]
fn coeff_rule_from_u8(n: u8) -> CoefficientCombineRule {
	match n {
		0 => Average,
		1 => Min,
		2 => Multiply,
		3 => Max,
		value => { godot_error!("Invalid CoefficientCombineRule: {}", value); Average }
	}
}