use rapier2d::{
	prelude::*,
	na::Unit,
	parry::{
		bounding_volume::BoundingSphere,
		query::{RayCast, PointQuery}
	}
};
use gdnative::{
	prelude::*,
	api::{CanvasItem, Resource, },
	thread_access::{ThreadAccess, LocalThreadAccess, NonUniqueThreadAccess}
};
use downcast_rs::{DowncastSync, impl_downcast};
use std::{
	f64::consts::TAU,
	sync::Arc,
	convert::TryFrom
};
use crate::{
	rapier2d::geometry::TypedShape,
	utils::{ToGd, AtomicProperty, ToRapier},
};

static DBG_DRAW_COLOR: Color = Color { r: 0.0, g: 1.0, b: 0.85, a: 0.2 };

macro_rules! shape_node {
	{|$T:ty| $($prop:ident : $t:ty = $def:expr => $path:literal ,)*} => {
		::paste::paste! {
			#[derive(NativeClass)]
			#[inherit(Resource)]
			#[register_with(Self::_bind_methods)]
			#[user_data(ArcData<Self>)]
			pub struct [<R2d $T>] {
				$(pub $prop : AtomicProperty<$t> ,)*
			}
			
			impl [<R2d $T>] {
				$(
					/// Shorthand for [AtomicProperty::load](crate::AtomicProperty)
					pub fn $prop(&self) -> $t {
						self.$prop.load()
					}
					
					/// Shorthand for [AtomicProperty::store(value)](crate::AtomicProperty)
					pub fn [<set_ $prop>](&self, value: $t) {
						self.$prop.store(value)
					}
				)*
			}
			
			impl Default for [<R2d $T>] {
				fn default() -> Self {
					Self {
						$($prop : AtomicProperty::new($def),)*
					}
				}
			}
			
			#[methods]
			impl [<R2d $T>] {
				fn new(_owner: &Resource) -> Self {
					Self::default()
				}
				
				fn _bind_methods(builder: &ClassBuilder<Self>) {
					$(
						builder.add_property::<$t>($path)
							.with_default($def)
							.with_getter(|this, _| {
							   this.$prop()
							})
							.with_shr_setter(|this, owner, value| {
								this.[<set_ $prop>](value);
								unsafe { owner.call("emit_changed", &[]) };
						  }).done();
					)*
				}
			}
		}
	};
}

/// Trait for shapes that can be drawn in Godot. Draws a red X by default.
pub trait Draw {
	fn draw(&self, at_node: &CanvasItem) {
		at_node.draw_line(
			Vector2::new(-10.0, 10.0),
			Vector2::new(10.0, -10.0),
			Color::from_rgba(1.0, 0.0, 0.0, 0.5),
			1.0,
			true,
		);
		at_node.draw_line(
			Vector2::new(-10.0, -10.0),
			Vector2::new(10.0, 10.0),
			Color::from_rgba(1.0, 0.0, 0.0, 0.5),
			1.0,
			true,
		);
	}
}

pub trait R2dShape: DowncastSync + Draw + Send + Sync {
	fn shared_shape(&self) -> SharedShape;
	#[cfg(feature = "default_nodes")]
	fn new_builder(&self) -> ColliderBuilder {
		ColliderBuilder::new(self.shared_shape())
	}
	fn is_selected_on_click(&self, point: Vector2, tolerance: f64) -> bool {
		point.length() as f64 <= tolerance
	}
}

impl_downcast!(sync R2dShape);

/// Trait for shapes that cannot be directly drawn in Godot and need a polygon
pub trait GetPoints {
	fn get_points(&self) -> Vector2Array {
		Vector2Array::new()
	}
}

pub struct ShapeInstance<Access: ThreadAccess = Shared> {
	pub(crate) shape: Arc<dyn R2dShape>,
	pub(crate) owner: Ref<Resource, Access>,
}

impl ShapeInstance<Unique> {
	pub fn into_shared(self) -> ShapeInstance<Shared> {
		ShapeInstance {
			shape: self.shape,
			owner: self.owner.into_shared(),
		}
	}
}

impl<Access: NonUniqueThreadAccess> Clone for ShapeInstance<Access> {
	fn clone(&self) -> Self {
		Self {
			shape: self.shape.clone(),
			owner: Ref::clone(&self.owner),
		}
	}
}

impl<Access: ThreadAccess> ShapeInstance<Access> {
	pub(crate) fn new(shape: Arc<dyn R2dShape>, owner: Ref<Resource, Access>) -> ShapeInstance<Access> {
		Self { shape, owner, }
	}
}

impl<Access: ThreadAccess> AsRef<Arc<dyn R2dShape>> for ShapeInstance<Access> {
	fn as_ref(&self) -> &Arc<dyn R2dShape> {
		&self.shape
	}
}

impl<Access: ThreadAccess> AsRef<Ref<Resource, Access>> for ShapeInstance<Access> {
	fn as_ref(&self) -> &Ref<Resource, Access> {
		&self.owner
	}
}

impl<Access: ThreadAccess> From<ShapeInstance<Access>> for Arc<dyn R2dShape> {
	fn from(inst: ShapeInstance<Access>) -> Self {
		inst.shape
	}
}

impl<Access: ThreadAccess> From<ShapeInstance<Access>> for Ref<Resource, Access> {
	fn from(input: ShapeInstance<Access>) -> Self {
		input.owner
	}
}

impl<Access: ThreadAccess> From<ShapeInstance<Access>> for SharedShape {
	fn from(inst: ShapeInstance<Access>) -> Self {
		inst.shape.shared_shape()
	}
}

impl<Access: LocalThreadAccess> TryFrom<Ref<Resource, Access>> for ShapeInstance<Access> {
	type Error = Ref<Resource, Access>;
	
	fn try_from(res: Ref<Resource, Access>) -> Result<Self, Self::Error> {
		try_cast_shape_instance(res)
	}
}

impl TryFrom<&SharedShape> for ShapeInstance<Unique> {
	/// id retrieved from [TypedShape::Custom](::rapier2d::geometry::TypedShape).
	type Error = u32;
	
	fn try_from(shape: &SharedShape) -> Result<Self, Self::Error> {
		use TypedShape::*;
		match shape.as_typed_shape() {
			Ball(shape) => {
				let (base, script) = Instance::emplace(R2dBall::from(shape)).decouple();
				Ok(ShapeInstance::new(script.into_inner(), base))
			}
			Cuboid(shape) => {
				let (base, script) = Instance::emplace(R2dCuboid::from(shape)).decouple();
				Ok(ShapeInstance::new(script.into_inner(), base))
			}
			Capsule(shape) => {
				let (base, script) = Instance::emplace(R2dRoundSegment::from(shape)).decouple();
				Ok(ShapeInstance::new(script.into_inner(), base))
			}
			Segment(shape) => {
				let (base, script) = Instance::emplace(R2dSegment::from(shape)).decouple();
				Ok(ShapeInstance::new(script.into_inner(), base))
			}
			Triangle(shape) => {
				let (base, script) = Instance::emplace(R2dTriangle::from(shape)).decouple();
				Ok(ShapeInstance::new(script.into_inner(), base))
			}
			TriMesh(_shape) => todo!("TriMesh"),
			Polyline(_shape) => todo!("Polyline"),
			HalfSpace(shape) => {
				let (base, script) = Instance::emplace(R2dHalfSpace::from(shape)).decouple();
				Ok(ShapeInstance::new(script.into_inner(), base))
			}
			HeightField(shape) => todo!("R2dHeightField::from({:?})", shape),
			Compound(_shape) => todo!("R2dCompound?"),
			ConvexPolygon(shape) => todo!("R2dConvexPolygon::from({:?})", shape),
			RoundCuboid(shape) => {
				let (base, script) = Instance::emplace(R2dRoundCuboid::from(shape)).decouple();
				Ok(ShapeInstance::new(script.into_inner(), base))
			}
			RoundTriangle(shape) => todo!("R2dRoundTriangle::from({:?})", shape),
			RoundConvexPolygon(shape) => todo!("R2dRoundConvexPolygon::from({:?})", shape),
			Custom(id) => Err(id),
		}
	}
}

impl<'r> TryFrom<TRef<'r, Resource, Shared>> for ShapeInstance<Shared> {
	type Error = Ref<Resource, Shared>;
	
	fn try_from(res: TRef<'r, Resource>) -> Result<Self, Self::Error> {
		try_cast_shape_instance(res.claim())
	}
}

fn try_cast_shape_instance<Access: ThreadAccess>(
	res: Ref<Resource, Access>
) -> Result<ShapeInstance<Access>, Ref<Resource, Access>> {
	
	fn try_shape<T, Access>(res: Ref<Resource, Access>) -> Result<ShapeInstance<Access>, Ref<Resource, Access>>
	where T: NativeClass<Base = Resource, UserData = ArcData<T>> + R2dShape,
				Access: ThreadAccess {
		res.try_cast_instance::<T>().map(|inst| {
			let (owner, script) = inst.decouple();
			let shape = script.into_inner() as Arc<dyn R2dShape>;
			ShapeInstance {
				owner,
				shape,
			}
		})
	}
	
	try_shape::<R2dBall, _>(res)
		.or_else(try_shape::<R2dSegment, _>)
		.or_else(try_shape::<R2dRoundSegment, _>)
		.or_else(try_shape::<R2dCapsuleX, _>)
		.or_else(try_shape::<R2dCapsuleY, _>)
		.or_else(try_shape::<R2dCuboid, _>)
		.or_else(try_shape::<R2dRoundCuboid, _>)
		.or_else(try_shape::<R2dTriangle, _>)
		.or_else(try_shape::<R2dHalfSpace, _>)
}

/// Draw default red X for ()
impl Draw for () {}

impl<T: Draw> Draw for Option<T> {
	fn draw(&self, at_node: &CanvasItem) {
		match self {
			Some(drawable) => drawable.draw(at_node),
			None => ().draw(at_node)
		}
	}
}

impl Draw for ShapeInstance {
	fn draw(&self, at_node: &CanvasItem) {
		self.shape.draw(at_node)
	}
}

shape_node! {
	|Ball|
	r: Real = 0.5 => "radius",
}

impl Draw for R2dBall {
	fn draw(&self, at_node: &CanvasItem) {
		at_node.draw_circle(Vector2::default(), self.r.load() as f64, DBG_DRAW_COLOR);
	}
}

impl R2dShape for R2dBall {
	fn shared_shape(&self) -> SharedShape {
		SharedShape::ball(self.r.load())
	}
	
	fn is_selected_on_click(&self, point: Vector2, tolerance: f64) -> bool {
		(point.length() as Real) < (self.r.load() + tolerance as Real)
	}
}

impl From<&Ball> for R2dBall {
	fn from(ball: &Ball) -> Self {
		Self {
			r: AtomicProperty::new(ball.radius)
		}
	}
}

shape_node! {
	|Segment|
	a: Vector2 = Vector2::ZERO => "a",
	b: Vector2 = Vector2::new(0.0, 10.0) => "b",
}

impl Draw for R2dSegment {
	fn draw(&self, at_node: &CanvasItem) {
		at_node.draw_line(
			self.a.load(),
			self.b.load(),
			DBG_DRAW_COLOR,
			2.0,
			true
		);
	}
}

impl R2dShape for R2dSegment {
	fn shared_shape(&self) -> SharedShape {
		let a = self.a.load().to_rapier();
		let b = self.b.load().to_rapier();
		SharedShape::segment(a, b)
	}
	
	fn is_selected_on_click(&self, point: Vector2, tolerance: f64) -> bool {
		let a = self.a.load();
		let b = self.b.load();
		
		let ab = b - a;
		let ap = point - a;
		let bp = point - b;
		
		match ab.dot(ap).partial_cmp(&ab.dot(bp)) {
			Some(std::cmp::Ordering::Greater) => bp.length() as f64 <= tolerance,
			_ => ap.length() as f64 <= tolerance,
		}
	}
}

impl From<&Segment> for R2dSegment {
	fn from(seg: &Segment) -> Self {
		Self {
			a: AtomicProperty::new(seg.a.to_gd()),
			b: AtomicProperty::new(seg.b.to_gd()),
		}
	}
}

shape_node! {
	|RoundSegment|
	a: Vector2 = Vector2::ZERO => "a",
	b: Vector2 = Vector2::new(0.0, 10.0) => "b",
	radius: Real = 0.0 => "radius",
}

impl Draw for R2dRoundSegment {}

impl R2dShape for R2dRoundSegment {
	fn shared_shape(&self) -> SharedShape {
		let a = self.a.load().to_rapier();
		let b = self.b.load().to_rapier();
		let radius = self.radius.load();
		SharedShape::capsule(a, b, radius)
	}
}

impl From<&Capsule> for R2dRoundSegment {
	fn from(cap: &Capsule) -> Self {
		Self {
			a: AtomicProperty::new(cap.segment.a.to_gd()),
			b: AtomicProperty::new(cap.segment.b.to_gd()),
			radius: AtomicProperty::new(cap.radius),
		}
	}
}

shape_node! {
	|CapsuleX|
	hh: Real = 10.0 => "hh",
	r: Real = 10.0 => "r",
}

impl GetPoints for R2dCapsuleX {
	fn get_points(&self) -> Vector2Array {
		let hh = self.hh.load() as f64;
		let r = self.r.load() as f64;
		
		let mut points = Vector2Array::new();
		points.resize(24);
		
		{
			let mut points = points.write();
			let points = points.get_mut(..).unwrap();
			
			for i in 0..12 {
				let th = (TAU / 4.0) - ((TAU / 24.0) * (i as f64));
				let x = f64::cos(th) * r + hh;
				let y = f64::sin(th) * r;
				points[i] = Vector2::new(x as f32, y as f32)
			}
			for i in 12..24 {
				let th = (TAU / 4.0) - ((TAU / 24.0) * (i as f64));
				let x = f64::cos(th) * r - hh;
				let y = f64::sin(th) * r;
				points[i] = Vector2::new(x as f32, y as f32)
			}
			//closes write when dropped
		}
		
		points
	}
}

impl Draw for R2dCapsuleX {
	fn draw(&self, at_node: &CanvasItem) {
		at_node.draw_colored_polygon(
			self.get_points(),
			DBG_DRAW_COLOR,
			Vector2Array::new(), Texture::null(), Texture::null(), true,
		)
	}
}

impl R2dShape for R2dCapsuleX {
	fn shared_shape(&self) -> SharedShape {
		let p = Point::from(Vector::x() * self.hh.load());
		SharedShape::capsule(-p, p, self.r.load())
	}
	
	fn is_selected_on_click(&self, point: Vector2, tolerance: f64) -> bool {
		let Vector2{ x, y, .. } = point;
		let hh = self.hh.load() as f32;
		let r = self.r.load() as f32;
		
		let r = r + tolerance as f32;
		
		if x > hh {
			let end = Vector2::new(hh, 0.0);
			let dist = (point - end).length();
			dist < r
		} else if x < -hh {
			let end = Vector2::new(-hh, 0.0);
			let dist = (point - end).length();
			dist < r
		} else {
			y > -r && y < r
		}
	}
}

shape_node! {
	|CapsuleY|
	hh: Real = 10.0 => "hh",
	r: Real = 10.0 => "r",
}

impl GetPoints for R2dCapsuleY {
	fn get_points(&self) -> Vector2Array {
		let hh = self.hh.load() as f64;
		let r = self.r.load() as f64;
		
		let mut points = Vector2Array::new();
		points.resize(24);
		
		{
			let mut points = points.write();
			let points = points.get_mut(..).unwrap();
			
			for i in 0..12 {
				let th = (TAU / 2.0) - ((TAU / 24.0) * (i as f64));
				let x = f64::cos(th) * r;
				let y = f64::sin(th) * r + hh;
				points[i] = Vector2::new(x as f32, y as f32)
			}
			
			for i in 12..24 {
				let th = (TAU / 2.0) - ((TAU / 24.0) * (i as f64));
				let x = f64::cos(th) * r;
				let y = f64::sin(th) * r - hh;
				points[i] = Vector2::new(x as f32, y as f32)
			}
		} // closes write when it goes out of scope
		
		points
	}
}

impl Draw for R2dCapsuleY {
	fn draw(&self, at_node: &CanvasItem) {
		at_node.draw_colored_polygon(
			self.get_points(),
			DBG_DRAW_COLOR,
			Vector2Array::new(), Texture::null(), Texture::null(), true,
		)
	}
}

impl R2dShape for R2dCapsuleY {
	fn shared_shape(&self) -> SharedShape {
		let p = Point::from(Vector::y() * self.hh.load());
		SharedShape::capsule(-p, p, self.r.load())
	}
	
	fn is_selected_on_click(&self, point: Vector2, tolerance: f64) -> bool {
		let Vector2{ x, y, .. } = point;
		let hh = self.hh.load() as f32;
		let r = self.r.load() as f32;
		
		let r = r + tolerance as f32;
		
		if x > hh {
			let end = Vector2::new(hh, 0.0);
			let dist = (point - end).length();
			dist < r
		} else if x < -hh {
			let end = Vector2::new(-hh, 0.0);
			let dist = (point - end).length();
			dist < r
		} else {
			y > -r && y < r
		}
	}
}

shape_node! {
	|Cuboid|
	hx: Real = 10.0 => "hx",
	hy: Real = 10.0 => "hy",
}

impl Draw for R2dCuboid {
	fn draw(&self, at_node: &CanvasItem) {
		let hx = self.hx.load();
		let hy = self.hy.load();
		at_node.draw_rect(
			Rect2 {
				position: Vector2::new(-hx as f32, -hy as f32),
				size: Vector2::new((hx * 2.0) as f32, (hy * 2.0) as f32),
			},
			DBG_DRAW_COLOR, true, 1.0, false,
		);
	}
}

impl R2dShape for R2dCuboid {
	fn shared_shape(&self) -> SharedShape {
		SharedShape::cuboid(self.hx.load(), self.hy.load())
	}
	
	fn is_selected_on_click(&self, point: Vector2, tolerance: f64) -> bool {
		let Vector2{ x, y, .. } = point;
		let hx = self.hx.load() as f32;
		let hy = self.hy.load() as f32;
		
		let (hx, hy) = (hx + tolerance as Real, hy + tolerance as Real);
		x < hx && x > -hx && y < hy && y > -hy
	}
}

impl From<&Cuboid> for R2dCuboid {
	fn from(cuboid: &Cuboid) -> Self {
		Self {
			hx: AtomicProperty::new(cuboid.half_extents.x),
			hy: AtomicProperty::new(cuboid.half_extents.y),
		}
	}
}

shape_node! {
	|RoundCuboid|
	hx: Real = 10.0 => "hx",
	hy: Real = 10.0 => "hy",
	r: Real = 2.0 => "r",
}

impl GetPoints for R2dRoundCuboid {
	fn get_points(&self) -> Vector2Array {
		let hx = self.hx.load();
		let hy = self.hy.load();
		let r = self.r.load();
		
		let mut points = Vector2Array::new();
		points.resize(24);
		
		{
			let mut points = points.write();
			let points = points.get_mut(..).unwrap();
			
			// top-right corner
			for i in 0..6 {
				let th = (TAU / 4.0) - ((TAU / 24.0) * (i as f64));
				let x = (th.cos() as f32 * r) + (hx);
				let y = (-th.sin() as f32 * r) - (hy);
				points[i] = Vector2::new(x, y);
			}
			
			// bottom-right corner
			for i in 6..12 {
				let th = (TAU / 4.0) - ((TAU / 24.0) * (i as f64));
				let x = (th.cos() as f32 * r) + (hx);
				let y = (-th.sin() as f32 * r) + (hy);
				points[i] = Vector2::new(x, y);
			}
			
			// bottom-left corner
			for i in 12..18 {
				let th = (TAU / 4.0) - ((TAU / 24.0) * (i as f64));
				let x = (th.cos() as f32 * r) - (hx);
				let y = (-th.sin() as f32 * r) + (hy);
				points[i] = Vector2::new(x, y);
			}
			
			// top-left corner
			for i in 18..24 {
				let th = (TAU / 4.0) - ((TAU / 24.0) * (i as f64));
				let x =(th.cos() as f32 * r) - (hx);
				let y = (-th.sin() as f32 * r) - (hy);
				points[i] = Vector2::new(x, y);
			}
		} // closes write when it goes out of scope
		
		points
	}
}

impl Draw for R2dRoundCuboid {
	fn draw(&self, at_node: &CanvasItem) {
		at_node.draw_colored_polygon(
			self.get_points(),
			DBG_DRAW_COLOR,
			Vector2Array::new(), Texture::null(), Texture::null(), true,
		)
	}
}

impl R2dShape for R2dRoundCuboid {
	fn shared_shape(&self) -> SharedShape {
		SharedShape::round_cuboid(self.hx.load(), self.hy.load(), self.r.load())
	}
	
	fn is_selected_on_click(&self, point: Vector2, tolerance: f64) -> bool {
		let Vector2{ x, y, .. } = point;
		let hx = self.hx.load();
		let hy = self.hy.load();
		let (hx, hy) = (hx + tolerance as Real, hy + tolerance as Real);
		x < hx && x > -hx && y < hy && y > -hy
	}
}

impl From<&RoundCuboid> for R2dRoundCuboid {
	fn from(round_cuboid: &RoundCuboid) -> Self {
		Self {
			hx: AtomicProperty::new(round_cuboid.base_shape.half_extents.x),
			hy: AtomicProperty::new(round_cuboid.base_shape.half_extents.y),
			r: AtomicProperty::new(round_cuboid.border_radius),
		}
	}
}

shape_node! {
	|Triangle|
	a: Vector2 = Vector2::new(0.0, -10.0) => "a",
	b: Vector2 = Vector2::new(10.0, 0.0) => "b",
	c: Vector2 = Vector2::ZERO => "c",
}

impl R2dShape for R2dTriangle {
	fn shared_shape(&self) -> SharedShape {
		let a = self.a.load().to_rapier();
		let b = self.b.load().to_rapier();
		let c = self.c.load().to_rapier();
		SharedShape::triangle(a, b, c)
	}
	
	fn is_selected_on_click(&self, point: Vector2, _tolerance: f64) -> bool {
		//FIXME: take tolerance in to account
		let v1 = self.a.load();
		let v2 = self.b.load();
		let v3 = self.c.load();
		
		// https://stackoverflow.com/a/2049593
		
		let sign = |a: Vector2, b: Vector2| {
			(point.x - b.x) * (a.y - b.y) - (a.x - b.x) * (point.y - b.y)
		};
		
		let d1 = sign(v1, v2);
		let d2 = sign(v2, v3);
		let d3 = sign(v3, v1);
		
		let has_neg = (d1 < 0.0) || (d2 < 0.0) || (d3 < 0.0);
		let has_pos = (d1 > 0.0) || (d2 > 0.0) || (d3 > 0.0);
		
		!(has_neg && has_pos)
	}
}

impl Draw for R2dTriangle {
	fn draw(&self, at_node: &CanvasItem) {
		at_node.draw_primitive(
			Vector2Array::from_slice(&[
				self.a.load(),
				self.b.load(),
				self.c.load(),
			]),
			ColorArray::from_slice(&[DBG_DRAW_COLOR; 3]),
			Vector2Array::new(),
			Texture::null(),
			1.0,
			Texture::null(),
		);
	}
}

impl From<&Triangle> for R2dTriangle {
	fn from(triangle: &Triangle) -> Self {
		Self {
			a: AtomicProperty::new(triangle.a.to_gd()),
			b: AtomicProperty::new(triangle.b.to_gd()),
			c: AtomicProperty::new(triangle.c.to_gd()),
		}
	}
}

#[derive(NativeClass, Default)]
#[inherit(Resource)]
#[register_with(Self::_bind_methods)]
#[user_data(ArcData<Self>)]
pub struct R2dHalfSpace {
	rotation: AtomicProperty<AngVector<Real>>,
}

#[methods]
impl R2dHalfSpace {
	fn new(_owner: &Resource) -> Self {
		Self::default()
	}

	fn _bind_methods(builder: &ClassBuilder<R2dHalfSpace>) {
		builder.add_property::<AngVector<Real>>("rotation_degrees")
			.with_default(AngVector::default())
			.with_getter(|this, _| {
				this.rotation.load().to_degrees()
			})
			.with_shr_setter(|this, owner, value| { unsafe {
				this.rotation.store((value as AngVector<Real>).to_radians(), );
				owner.call("emit_changed", &[])
			}; })
			.done();
	}
}

impl Draw for R2dHalfSpace {
	fn draw(&self, at_node: &CanvasItem) {
		// even if we figured out how big this needs to be to cover the current viewport, it won't redraw when the camera moves
		let rect = Rect2 {
			position: Vector2::new(-1_000_000.0, -1_000_000.0),
			size: Vector2::new(1_000_000.0, 2_000_000.0),
		};
		at_node.draw_set_transform(
			Vector2::ZERO,
			self.rotation.load() as f64,
			Vector2::ONE,
		);
		at_node.draw_rect(rect, DBG_DRAW_COLOR, true, 1.0, false);
	}
}

impl R2dShape for R2dHalfSpace {
	fn shared_shape(&self) -> SharedShape {
		let rad = self.rotation.load();
		let normal = Unit::new_unchecked(Vector::new(rad.cos(), rad.sin()));
		let hs = HalfSpace::new(normal);
		SharedShape::new(hs)
	}
	
	fn is_selected_on_click(&self, point: Vector2, tolerance: f64) -> bool {
		(point.rotated(-self.rotation.load()).x as f64) < tolerance
	}
}

impl From<&HalfSpace> for R2dHalfSpace {
	fn from(half_space: &HalfSpace) -> Self {
		Self {
			rotation: AtomicProperty::new(half_space.normal.angle(&Vector::x()))
		}
	}
}

/// [ColliderShape](::r2d::geometry::ColliderShape) for custom Collider nodes with no `shape` resource set.
#[derive(Copy, Clone, Default, Debug, PartialEq, Eq, Hash)]
pub struct PlaceholderShape;

impl RayCast for PlaceholderShape {
	fn cast_local_ray_and_get_normal(&self, _ray: &Ray, _max_toi: f32, _solid: bool) -> Option<RayIntersection> {
		None
	}
}

impl PointQuery for PlaceholderShape {
	fn project_local_point(&self, pt: &Point<f32>, _solid: bool) -> PointProjection {
		self.project_local_point_and_get_feature(pt).0
	}
	
	fn project_local_point_and_get_feature(&self, _pt: &Point<f32>) -> (PointProjection, FeatureId) {
		(
			PointProjection {
				is_inside: false,
				point: Point::new(0.0, 0.0),
			},
			FeatureId::Unknown,
		)
	}
}

impl Shape for PlaceholderShape {
	fn compute_local_aabb(&self) -> AABB {
		AABB::new(Point::new(0.0, 0.0), Point::new(0.0, 0.0))
	}
	
	fn compute_local_bounding_sphere(&self) -> BoundingSphere {
		BoundingSphere::new(Point::new(0.0, 0.0), 0.0)
	}
	
	fn clone_box(&self) -> Box<dyn Shape> {
		Box::new(*self)
	}
	
	fn mass_properties(&self, _density: f32) -> MassProperties {
		MassProperties::new(Point::new(0.0, 0.0), 0.0, 0.0)
	}
	
	fn shape_type(&self) -> ShapeType {
		ShapeType::Custom
	}
	
	fn as_typed_shape(&self) -> TypedShape {
		TypedShape::Custom(u32::MAX)
	}
	
	fn ccd_thickness(&self) -> f32 {
		0.0
	}
	
	fn ccd_angular_thickness(&self) -> f32 {
		0.0
	}
}