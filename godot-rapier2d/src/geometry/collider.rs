use rapier2d::prelude::*;
use crate::{traits::*, geometry::{
	material::R2dColliderMaterial,
	shapes::{ShapeInstance, PlaceholderShape, },
}, utils::*};
use std::convert::TryFrom;
use gdnative::{
	nativescript::{
		property::{IntHint, EnumHint, },
		Map, MapMut, Export,
	},
	prelude::*,
	api::Resource,
};

pub type ColliderBase = Node2D;

components! {
	#[derive(Clone)]
	struct Collider {
		collider_type: ColliderType = ColliderType::Solid,
		shape_instance: Option<ShapeInstance> = None,
		position: ColliderPosition,
		material_instance: Option<Instance<R2dColliderMaterial, Shared>> = None,
		flags: ColliderFlags,
		mass_props: ColliderMassProps,
		changes: ColliderChanges,
		broad_phase_data: ColliderBroadPhaseData,
	}
}

//TODO: should material just be a normal group of properties? Nice to share it, but harder to marshal
//todo: mass_props
properties! {
	|Collider|
	collider_type_index: u64 => (*)collider_type;
	active_collision_types: ActiveCollisionTypesProxy => flags.active_collision_types;
	collision_group_memberships: u32 => flags.collision_groups.memberships;
	collision_group_filter: u32 => flags.collision_groups.filter;
	active_hooks: u32 => flags.active_hooks;
	active_events: u32 => flags.active_events;
}

impl<T: R2dNode<ColliderComponents>> ColliderComponentAccessors for T {}

/// Trait for accessing Godot [Resource](::gdnative::api::Resource)-based properties on `Collider`s.
///
/// Separate from [ColliderComponentAccessors] due to the tricky nature of keeping `Resource`s alive
/// when the components don't store them directly.
pub trait ColliderResourceAccessors: R2dNode<ColliderComponents> {
	fn shape(&self) -> &Option<ShapeInstance> {
		&self.components().shape_instance
	}
	
	fn set_shape(&mut self, shape: Option<ShapeInstance>)
	where Self: SetComponent<ColliderShape> {
		self.apply_to_shape(|shape_component| *shape_component = shape)
	}
	
	fn apply_to_shape<Ret>(&mut self, f: impl FnOnce(&mut Option<ShapeInstance>) -> Ret) -> Ret
	where Self: SetComponent<ColliderShape> {
		let ret = f(&mut self.components_mut().shape_instance);
		self.set_component(
			self.components().shape_instance.as_ref().map_or(
				ColliderShape::new(PlaceholderShape),
				|inst| inst.shape.shared_shape()
			)
		);
		ret
	}
	
	fn material(&self) -> &Option<Instance<R2dColliderMaterial, Shared>> {
		&self.components().material_instance
	}
	
	fn set_material(&mut self, material: Option<Instance<R2dColliderMaterial, Shared>>)
	where Self: SetComponent<ColliderMaterial> {
		self.apply_to_material(|material_component| *material_component = material)
	}
	
	fn apply_to_material<Ret>(&mut self, f: impl FnOnce(&mut Option<Instance<R2dColliderMaterial, Shared>>) -> Ret) -> Ret
	where Self: SetComponent<ColliderMaterial> {
		let ret = f(&mut self.components_mut().material_instance);
		self.set_component(self.components().material_instance.to_rapier());
		ret
	}
}

impl<T> ColliderResourceAccessors for T
where T: R2dNode<ColliderComponents> {}

impl<T: NativeClass + R2dNode<ColliderComponents>> ColliderProperties for T
where <T as NativeClass>::UserData: Map + MapMut,
      <T as NativeClass>::Base: SubClass<ColliderBase> + SubClass<Object>,
      T: SetColliderComponents + R2dNode<ColliderComponents> {
	fn register_r2d_properties(builder: &ClassBuilder<Self>) where Self: Sized {
		let default = ColliderComponents::default();
		let prefix = T::property_prefix();
		
		builder.add_property::<Option<Ref<Resource, Shared>>>("shape")
			.with_default(None)
			.with_getter(|t: &T, _| t.shape_instance().as_ref().map(|inst| inst.owner.clone()))
			.with_setter(|t: &mut T, owner, value: Option<Ref<Resource, Shared>>| {
				let inst = value
					.map(|value| ShapeInstance::try_from(unsafe { value.assume_safe() }))
					.transpose();
				match inst {
					Ok(new_inst) => {
						unsafe {
							t.apply_to_shape(|inst_component| {
								let old_inst = std::mem::replace(inst_component, new_inst);
								if let Some(old_inst) = old_inst {
									let shape_owner = old_inst.owner.assume_safe();
									shape_owner.disconnect("changed", owner, "_shape_changed");
								};
								inst_component.as_ref().map(|inst_component| inst_component.owner.assume_safe()
									.connect("changed", owner, "_shape_changed", VariantArray::new_shared(), 0)
									.expect("failed to connect signal `changed`")
								);
							});
							owner.upcast::<Object>().call_deferred("_shape_changed", &[]);
						}
					}
					Err(res) => godot_error!("failed to set ShapeInstance: {:?}", res)
				}
			})
			.done();
		
		builder.add_property::<Option<Instance<R2dColliderMaterial, Shared>>>("material")
			.with_default(None)
			.with_getter(|t: &T, _| t.material().clone())
			.with_setter(|t: &mut T, _, value: Option<Instance<R2dColliderMaterial, Shared>>| {
				t.set_material(value);
			})
			.done();
		
		macro_rules! prop {
			($prop:ident : $t:ty $({$hint:expr})? $(, $usage:expr)? => $path:literal) => {
				prop!($prop : $t $({$hint})? = default.$prop() $(, $usage)? => $path);
			};
			($prop:ident : $t:ty $({$hint:expr})? = $def:expr $(, $usage:expr)? => $path:literal) => {
				builder.add_property::<$t>(&*(prefix.clone() + $path))
					$(.with_hint($hint))?
					.with_default($def)
					.with_getter(|t: &T, _| t.$prop())
					.with_setter(|t: &mut T, _, value| ::paste::paste!(t.[<set_ $prop>](value)))
					.done();
			};
		}
		
		macro_rules! props {
			($($prop:ident : $t:ty $({$hint:expr})? $(= $def:expr)? $(, $usage:expr)? => $path:literal ;)*) => {
				$(prop!($prop : $t $({$hint})? $(= $def)? => $path);)*
			}
		}
		
		//TODO: mass_props
		props! {
			collider_type_index: u64 { IntHint::Enum(EnumHint::new(vec![
				"Solid".into(),
				"Sensor".into(),
			]))} => "collider_type";
			active_collision_types: ActiveCollisionTypesProxy { IntHint::Flags(EnumHint::new(vec![
				"Dynamic <=> Dynamic".to_string(),
				"Dynamic <=> Static".to_string(),
				"Dynamic <=> Kinematic".to_string(),
				"Kinematic <=> Kinematic".to_string(),
				"Kinematic <=> Static".to_string(),
				"Static <=> Static".to_string(),
			]))} => "flags/active_collision_types";
			collision_group_memberships: u32 => "flags/collision_groups/memberships";
			collision_group_filter: u32 => "flags/collision_groups/filter";
			active_hooks: u32 => "flags/active_hooks";
			active_events: u32 => "flags/active_events";
		}
	}
	
	fn override_set_position_and_rotation(&mut self, owner: &ColliderBase, property: GodotString, value: Variant) -> bool{
		if property == GLOBAL_POS_NAME.new_ref() {
			let vec = value.to_vector2();
			owner.upcast::<Node2D>().set_global_position(vec);
			self.apply_to_position(|pos| pos.translation = vec.to_rapier());
			true
		} else if property == GLOBAL_ROT_NAME.new_ref() {
			let rot = value.to_f64();
			owner.upcast::<Node2D>().set_global_rotation(rot);
			self.apply_to_position(|pos| pos.rotation = (rot as Real).to_rapier());
			true
		} else if property == LOCAL_POS_NAME.new_ref() {
			let vec = value.to_vector2();
			owner.upcast::<Node2D>().set_position(vec);
			let new_pos = owner.upcast::<Node2D>().global_position();
			self.apply_to_position(|pos| pos.translation = new_pos.to_rapier());
			true
		} else if property == LOCAL_ROT_NAME.new_ref() {
			let rot = value.to_f64();
			owner.upcast::<Node2D>().set_rotation(rot);
			let new_rot = owner.upcast::<Node2D>().global_rotation();
			self.apply_to_position(|pos| pos.rotation = (new_rot as Real).to_rapier());
			true
		} else {
			false
		}
	}
}

bitflags::bitflags!(
	/// Turns Rapier's ActiveCollisionTypes flags into a basic sequential set of flags that Godot can
	/// interpret as boolean flags for the editor.
	pub struct ActiveCollisionTypesProxy: u8 {
		const DYNAMIC_DYNAMIC = 0b0000_0001;
		const DYNAMIC_KINEMATIC = 0b0000_0100;
		const DYNAMIC_STATIC = 0b0000_0010;
		const KINEMATIC_KINEMATIC = 0b0000_1000;
		const KINEMATIC_STATIC = 0b0001_0000;
		const STATIC_STATIC = 0b0010_0000;
	}
);

impl ToVariant for ActiveCollisionTypesProxy {
	fn to_variant(&self) -> Variant {
		self.bits().to_variant()
	}
}

impl FromVariant for ActiveCollisionTypesProxy {
	fn from_variant(variant: &Variant) -> Result<Self, FromVariantError> {
		if let Some(bits) = variant.try_to_u64() {
			let bits = bits as u8;
			Self::from_bits(bits).map_or(
				Err(FromVariantError::Custom(format!("invalid ActiveCollisionTypesProxy bits: {:b}", bits))),
				Ok
			)
		} else {
			return Err(FromVariantError::InvalidVariantType {
				variant_type: variant.get_type(),
				expected: VariantType::I64
			})
		}
	}
}

impl Default for ActiveCollisionTypesProxy {
	fn default() -> Self {
		ActiveCollisionTypesProxy::DYNAMIC_DYNAMIC
			| ActiveCollisionTypesProxy::DYNAMIC_KINEMATIC
			| ActiveCollisionTypesProxy::DYNAMIC_STATIC
	}
}

impl Export for ActiveCollisionTypesProxy {
	type Hint = IntHint<u8>;
	
	fn export_info(hint: Option<Self::Hint>) -> ExportInfo {
		hint.map_or_else(
			|| { godot_error!("need IntHint::Flags"); ExportInfo::new(VariantType::I64) },
			|hint| hint.export_info()
		)
	}
}

impl CloneFromRapier<ActiveCollisionTypes> for ActiveCollisionTypesProxy {
	fn clone_from_rapier(&input: &ActiveCollisionTypes) -> Self {
		// directly set bits that match positions
		let mut ret = ActiveCollisionTypesProxy::from_bits((input & (
			ActiveCollisionTypes::DYNAMIC_DYNAMIC | ActiveCollisionTypes::DYNAMIC_STATIC | ActiveCollisionTypes::STATIC_STATIC
		)).bits() as u8).unwrap();
		ret.set(ActiveCollisionTypesProxy::DYNAMIC_KINEMATIC, input.contains(ActiveCollisionTypes::DYNAMIC_KINEMATIC));
		ret.set(ActiveCollisionTypesProxy::KINEMATIC_KINEMATIC, input.contains(ActiveCollisionTypes::KINEMATIC_KINEMATIC));
		ret.set(ActiveCollisionTypesProxy::KINEMATIC_STATIC, input.contains(ActiveCollisionTypes::STATIC_STATIC));
		ret
	}
}

impl CloneFromGd<ActiveCollisionTypesProxy> for ActiveCollisionTypes {
	fn clone_from_gd(&input: &ActiveCollisionTypesProxy) -> Self {
		// directly set bits that match positions
		let mut ret = ActiveCollisionTypes::from_bits((input & (
			ActiveCollisionTypesProxy::DYNAMIC_DYNAMIC | ActiveCollisionTypesProxy::DYNAMIC_STATIC | ActiveCollisionTypesProxy::STATIC_STATIC
		)).bits() as u16).unwrap();
		ret.set(ActiveCollisionTypes::DYNAMIC_KINEMATIC, input.contains(ActiveCollisionTypesProxy::DYNAMIC_KINEMATIC));
		ret.set(ActiveCollisionTypes::KINEMATIC_KINEMATIC, input.contains(ActiveCollisionTypesProxy::KINEMATIC_KINEMATIC));
		ret.set(ActiveCollisionTypes::KINEMATIC_STATIC, input.contains(ActiveCollisionTypesProxy::STATIC_STATIC));
		ret
	}
}
