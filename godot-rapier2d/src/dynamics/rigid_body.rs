use paste::paste;
use rapier2d::prelude::*;

use gdnative::{
	nativescript::{
		property::{EnumHint, IntHint, Usage},
		Map, MapMut,
	},
	prelude::*,
};

use crate::{traits::{SetComponent, SetRigidBodyComponents, }, utils::*, R2dNode};

pub type RigidBodyBase = Node2D;

components! {
	#[derive(Clone, Debug)]
	struct RigidBody {
		rigid_body_type: RigidBodyType = RigidBodyType::Dynamic,
		position: RigidBodyPosition,
		velocity: RigidBodyVelocity,
		mass_props: RigidBodyMassProps,
		forces: RigidBodyForces,
		activation: RigidBodyActivation,
		damping: RigidBodyDamping,
		dominance: RigidBodyDominance,
		ccd: RigidBodyCcd,
		changes: RigidBodyChanges,
		ids: RigidBodyIds,
		colliders: RigidBodyColliders,
	}
}

//TODO: position, mass_props, dominance,
properties! {
	|RigidBody|
	body_type: u64 => (*)rigid_body_type;
	next_position: Vector2 => position.next_position.translation;
	next_rotation: Real => position.next_position.rotation;
	linvel: Vector2 => velocity.linvel;
	angvel: Real => velocity.angvel;
	force: Vector2 => forces.force;
	torque: Real => forces.torque;
	gravity_scale: Real => forces.gravity_scale;
	activation_threshold: Real => activation.threshold;
	activation_energy: Real => activation.energy;
	sleeping: bool => activation.sleeping;
	linear_damping: Real => damping.linear_damping;
	angular_damping: Real => damping.angular_damping;
	dominance: i8 => (*)dominance;
	ccd_thickness: Real => ccd.ccd_thickness;
	ccd_max_dist: Real => ccd.ccd_max_dist;
	ccd_active: bool => ccd.ccd_active;
	ccd_enabled: bool => ccd.ccd_enabled;
}

impl<T: R2dNode<RigidBodyComponents>> RigidBodyComponentAccessors for T {}

impl<T: NativeClass> RigidBodyProperties for T
where <T as NativeClass>::UserData: Map + MapMut,
      <T as NativeClass>::Base: SubClass<RigidBodyBase>,
      T: SetRigidBodyComponents + R2dNode<RigidBodyComponents> {
	fn register_r2d_properties(builder: &ClassBuilder<T>) {
		let default = RigidBodyComponents::default();
		let prefix = T::property_prefix();
		
		macro_rules! prop {
			($prop:ident : $t:ty $({$hint:expr})? $(, $usage:expr)? => $path:literal) => {
				prop!($prop : $t $({$hint})? = default.$prop() $(, $usage)? => $path)
			};
			($prop:ident : $t:ty $({$hint:expr})? = $def:expr $(, $usage:expr)? => $path:literal) => {
				builder.add_property::<$t>(&*(prefix.clone() + $path))
					.with_default($def)$(
					.with_hint($hint))?$(
					.with_usage($usage))?
					.with_getter(|t: &T, _| t.$prop())
					.with_setter(|t: &mut T, _, value| paste!(t.[<set_ $prop>](value)))
					.done();
			};
		}
		
		macro_rules! props {
			($($prop:ident : $t:ty $({$hint:expr})? $(= $def:expr)? $(, $usage:expr)? => $path:literal ;)*) => {
				$(prop!($prop : $t $({$hint})? $(= $def)? $(, $usage)? => $path);)*
			}
		}
		
		props! {
			body_type: u64 { IntHint::Enum(EnumHint::new(vec![
				"Dynamic".into(),
				"Static".into(),
				"KinematicPositionBased".into(),
				"KinematicVelocityBased".into(),
			]))} => "body_type";
			next_position: Vector2, Usage::NOEDITOR => "next_position";
			next_rotation: Real, Usage::NOEDITOR =>"next_rotation";
			linvel: Vector2 => "velocity/linear";
			angvel: Real => "velocity/angular";
			force: Vector2 => "forces/force";
			torque: Real => "forces/torque";
			gravity_scale: Real => "forces/gravity_scale";
			activation_threshold: Real => "activation/threshold";
			activation_energy: Real => "activation/energy";
			sleeping: bool => "activation/sleeping";
			linear_damping: Real => "damping/linear";
			angular_damping: Real => "damping/angular";
			ccd_thickness: Real => "ccd/thickness";
			ccd_max_dist: Real => "ccd/max_dist";
			ccd_active: bool => "ccd/active";
			ccd_enabled: bool => "ccd/enabled";
		}
	}
	
	fn override_set_position_and_rotation(&mut self, owner: &RigidBodyBase, property: GodotString, value: Variant) -> bool {
		if property == GLOBAL_POS_NAME.new_ref() {
			let vec = value.to_vector2();
			owner.upcast::<Node2D>().set_global_position(vec);
			self.apply_to_position(|pos| pos.position.translation = vec.to_rapier());
			true
		} else if property == GLOBAL_ROT_NAME.new_ref() {
			let rot = value.to_f64();
			owner.upcast::<Node2D>().set_global_rotation(rot);
			self.apply_to_position(|pos| pos.position.rotation = (rot as Real).to_rapier());
			true
		} else if property == LOCAL_POS_NAME.new_ref() {
			let vec = value.to_vector2();
			owner.upcast::<Node2D>().set_position(vec);
			let new_pos = owner.upcast::<Node2D>().global_position();
			self.apply_to_position(|pos| pos.position.translation = new_pos.to_rapier());
			true
		} else if property == LOCAL_ROT_NAME.new_ref() {
			let rot = value.to_f64();
			owner.upcast::<Node2D>().set_rotation(rot);
			let new_rot = owner.upcast::<Node2D>().global_rotation();
			self.apply_to_position(|pos| pos.position.rotation = (new_rot as Real).to_rapier());
			true
		} else {
			false
		}
	}
}