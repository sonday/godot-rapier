use gdnative::{
	prelude::*,
	api::Resource,
};
use rapier2d::{prelude::*, na::Unit, };
use crate::utils::AtomicProperty;

macro_rules! prop {
	($field:ident, $name:literal, $t:ty, $def:expr, $builder:ident) => {
		$builder.add_property::<$t>($name)
			.with_default($def)
			.with_getter(|this, _| {
			   this.$field.load()
			})
			.with_shr_setter(|this, _owner, value| {
				this.$field.store(value);
			}).done();
	}
}

#[derive(NativeClass, Default)]
#[inherit(Resource)]
#[register_with(Self::_bind_methods)]
#[user_data(ArcData<Self>)]
pub struct R2dBallJoint {
	local_anchor1: AtomicProperty<Vector2>,
	local_anchor2: AtomicProperty<Vector2>,
}

#[methods]
impl R2dBallJoint {
	fn new(_owner: &Resource) -> Self {
		Self::default()
	}
	
	fn _bind_methods(builder: &ClassBuilder<Self>) {
		prop!(local_anchor1, "local_anchor1", Vector2, Vector2::ZERO, builder);
		prop!(local_anchor2, "local_anchor2", Vector2, Vector2::ZERO, builder);
	}
	
	pub fn params(&self) -> JointParams {
		let la1 = self.local_anchor1.load();
		let la2 = self.local_anchor2.load();
		
		let la1 = Point::new(la1.x, la1.y);
		let la2 = Point::new(la2.x, la2.y);
		
		BallJoint::new(la1, la2).into()
	}
}

#[derive(NativeClass, Default)]
#[inherit(Resource)]
#[register_with(Self::_bind_methods)]
#[user_data(ArcData<Self>)]
pub struct R2dFixedJoint {
	local_translation1: AtomicProperty<Vector2>,
	local_rotation1: AtomicProperty<Real>,
	local_translation2: AtomicProperty<Vector2>,
	local_rotation2: AtomicProperty<Real>,
}

#[methods]
impl R2dFixedJoint {
	fn new(_owner: &Resource) -> Self {
		Self::default()
	}
	
	fn _bind_methods(builder: &ClassBuilder<Self>) {
		prop!(local_translation1, "local_anchor1/translation", Vector2,Vector2::ZERO, builder);
		prop!(local_rotation1, "local_anchor1/rotation", Real, 0.0, builder);
		prop!(local_translation2, "local_anchor2/translation", Vector2, Vector2::ZERO, builder);
		prop!(local_rotation2, "local_anchor2/rotation", Real, 0.0, builder);
	}
	
	pub fn params(&self) -> JointParams {
		let Vector2{ x: x1, y: y1, } = self.local_translation1.load();
		let Vector2{ x: x2, y: y2, } = self.local_translation2.load();
		
		let local_anchor1 = Isometry::new(
			Vector::new(x1, y1),
			self.local_rotation1.load() as Real,
		);
		let local_anchor2 = Isometry::new(
			Vector::new(x2, y2),
			self.local_rotation2.load() as Real,
		);
		
		FixedJoint::new(local_anchor1, local_anchor2).into()
	}
}



#[derive(NativeClass, Default)]
#[inherit(Resource)]
#[register_with(Self::_bind_methods)]
#[user_data(ArcData<Self>)]
pub struct R2dPrismaticJoint {
	local_anchor1: AtomicProperty<Vector2>,
	local_axis1: AtomicProperty<Vector2>,
	
	local_anchor2: AtomicProperty<Vector2>,
	local_axis2: AtomicProperty<Vector2>,
}

#[methods]
impl R2dPrismaticJoint {
	fn new(_owner: &Resource) -> Self {
		Self {
			local_anchor1: AtomicProperty::new(Vector2::ZERO),
			local_axis1: AtomicProperty::new(Vector2::new(1.0, 0.0)),
			local_anchor2: AtomicProperty::new(Vector2::ZERO),
			local_axis2: AtomicProperty::new(Vector2::new(1.0, 0.0)),
		}
	}
	
	fn _bind_methods(builder: &ClassBuilder<R2dPrismaticJoint>) {
		prop!(local_anchor1, "local_anchor1", Vector2, Vector2::ZERO, builder);
		prop!(local_axis1, "local_axis1", Vector2, Vector2::ZERO, builder);
		prop!(local_anchor2, "local_anchor2", Vector2, Vector2::ZERO, builder);
		prop!(local_axis2, "local_axis2", Vector2, Vector2::ZERO, builder);
	}
	
	pub fn params(&self) -> JointParams {
		let local_anchor1 =  self.local_anchor1.load();
		let local_axis1 =  self.local_axis1.load();
		
		let local_anchor2 =  self.local_anchor2.load();
		let local_axis2 =  self.local_axis2.load();
		
		let local_anchor1 = Point::new(local_anchor1.x, local_anchor1.y);
		let local_axis1 = Unit::new_normalize(Vector::new(local_axis1.x, local_axis1.y));
		
		let local_anchor2 = Point::new(local_anchor2.x, local_anchor2.y);
		let local_axis2 = Unit::new_normalize(Vector::new(local_axis2.x, local_axis2.y));
		
		PrismaticJoint::new(
			local_anchor1,
			local_axis1,
			local_anchor2,
			local_axis2,
		).into()
	}
}
