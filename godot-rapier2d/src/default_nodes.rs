mod pipeline;
mod server;
mod rigid_body;
mod collider;
mod joint;

pub use collider::*;
pub use joint::*;
pub use pipeline::*;
pub use rigid_body::*;
pub use server::*;

use gdnative::nativescript::user_data::{DeadlockPolicy, LockOptions};
use std::time::Duration;

pub struct R2dDeadlockPolicy;

impl LockOptions for R2dDeadlockPolicy {
	const DEADLOCK_POLICY: DeadlockPolicy = DeadlockPolicy::Timeout(Duration::from_millis(2));
}
