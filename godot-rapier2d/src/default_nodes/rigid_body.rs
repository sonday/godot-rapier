use gdnative::{
	api::Engine,
	prelude::*,
};
use rapier2d::{
	data::ComponentSetMut,
	prelude::*,
};

use crate::{
	default_nodes::collider::R2dCollider,
	dynamics::rigid_body::*,
	traits::R2dNode,
	utils::{GetIsometry, ToRapier},
};
use crate::default_nodes::R2dDeadlockPolicy;

use super::server::R2dServer;
use crate::traits::MapStorageMut;

#[derive(NativeClass)]
#[inherit(Node2D)]
#[register_with(Self::register_r2d_properties)]
#[user_data(RwLockData<Self, R2dDeadlockPolicy>)]
pub struct R2dRigidBody {
	properties: RigidBodyComponents,
	pub(crate) handle: RigidBodyHandle,
	#[property(default = false)]
	pub sync_to_physics: bool,
	in_tree: bool,
}

#[methods]
impl R2dRigidBody {
	fn new(_owner: &Node2D) -> Self {
		Self {
			properties: RigidBodyComponents::new(),
			handle: RigidBodyHandle::invalid(),
			sync_to_physics: false,
			in_tree: false,
		}
	}
	
	pub fn make_builder(&self) -> rapier2d::dynamics::RigidBodyBuilder {
		use rapier2d::{
			dynamics::RigidBodyBuilder,
		};
		
		let builder = match self.rigid_body_type() {
			RigidBodyType::Dynamic => RigidBodyBuilder::new_dynamic(),
			RigidBodyType::Static => RigidBodyBuilder::new_static(),
			RigidBodyType::KinematicPositionBased => RigidBodyBuilder::new_kinematic_position_based(),
			RigidBodyType::KinematicVelocityBased => RigidBodyBuilder::new_kinematic_velocity_based(),
		};
		
		builder
			.position(self.position().position)
			.linvel(self.properties.linvel().to_rapier())
			.angvel(self.velocity().angvel)
			.gravity_scale(self.forces().gravity_scale)
			.linear_damping(self.damping().linear_damping)
			.angular_damping(self.damping().angular_damping)
			.additional_mass_properties(self.mass_props().local_mprops)
			.can_sleep(self.activation().threshold >= 0.0)
			.sleeping(self.activation().sleeping)
	}
	
	#[export]
	fn _enter_tree(&mut self, owner: TRef<Node2D>) {
		let position =  owner.global_isometry();
		let rb_pos = RigidBodyPosition {
			position,
			next_position: position,
		};
		self.set_position(rb_pos);
		if !Engine::godot_singleton().is_editor_hint() {
			R2dServer::singleton().insert_rigid_body(self, owner);
			self.in_tree = true;
		}
	}
	
	#[export]
	fn _get_configuration_warning(&self, owner: &Node2D) -> &'static str {
		for child in owner.get_children().iter() {
			if let Some(child) = child.try_to_object::<Node2D>() {
				if let Some(_) = unsafe { child.assume_safe() }.cast_instance::<R2dCollider>() {
					return ""
				}
			}
		}
		"There should be at least one R2dCollider as a child of this R2dBody"
	}
	
	#[export]
	fn _exit_tree(&mut self, owner: TRef<Node2D, Shared>) {
		if !Engine::godot_singleton().is_editor_hint() {
			let _body = R2dServer::singleton().remove_body(self, owner);
			self.handle = RigidBodyHandle::invalid();
			self.in_tree = false;
		}
	}
}

impl AsRef<RigidBodyComponents> for R2dRigidBody {
	#[inline]
	fn as_ref(&self) -> &RigidBodyComponents {
		&self.properties
	}
}

impl AsMut<RigidBodyComponents> for R2dRigidBody {
	#[inline]
	fn as_mut(&mut self) -> &mut RigidBodyComponents {
		&mut self.properties
	}
}

impl R2dNode<RigidBodyComponents> for R2dRigidBody {
	#[inline]
	fn components(&self) -> &RigidBodyComponents {
		self.as_ref()
	}
	
	#[inline]
	fn components_mut(&mut self) -> &mut RigidBodyComponents {
		self.as_mut()
	}
}

impl<C, Ret> MapStorageMut<C, Ret> for R2dRigidBody
where RigidBodySet: ComponentSetMut<C> {
	#[inline]
	fn map_storage_mut(&mut self, f: impl for<'c> FnOnce(&'c mut C) -> Ret + Send + Sync + 'static) -> () {
		if self.in_tree {
			R2dServer::singleton()
				.mut_bodies(|bodies| bodies.map_mut_internal(self.handle.0, f))
				.or_else(|| { godot_error!("failed to set component {}", std::any::type_name::<C>()); None });
		}
	}
}