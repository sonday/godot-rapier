use atomic_refcell::AtomicRefCell;
use gdnative::{
	core_types::Vector2,
	prelude::*,
	api::Engine,
};
use rapier2d::prelude::*;
use once_cell::sync::Lazy;
use std::collections::{HashMap, HashSet};
use crate::{
	dynamics::rigid_body::RigidBodyBase,
	default_nodes::{
		pipeline::*,
		joint::*,
		collider::R2dCollider,
		rigid_body::R2dRigidBody
	},
	utils::GetIsometry,
};

#[derive(Default)]
struct R2dServerInner {
	pipeline: R2dPipeline,
	bodies_to_sync_to_physics: HashSet<RigidBodyHandle>,
	
	body_nodes: HashMap<RigidBodyHandle, Ref<RigidBodyBase, Shared>>,
	collider_nodes: HashMap<ColliderHandle, Ref<<R2dCollider as NativeClass>::Base, Shared>>,
	joint_nodes: HashMap<JointHandle, Ref<<R2dJoint as NativeClass>::Base, Shared>>,
}

impl R2dServerInner {
	pub fn step(&mut self, owner: &Node) {
		// self.make_room_for_events();
		owner.emit_signal(R2dServer::PRE_STEP, &[]);
		self.pipeline.step();
		self.update_godot_xforms();
		owner.emit_signal(R2dServer::STEPPED, &[]);
	}
	
	fn update_godot_xforms(&self) {
		for (&handle, node) in &self.body_nodes {
			let pos = self.pipeline.body(handle)
				.expect(&*format!("missing body {:?}", handle)).position();
			let tr = pos.translation;
			let node = unsafe { node.assume_safe() };
			node.set_global_position(Vector2::new(tr.x as f32, tr.y as f32));
			node.set_global_rotation(pos.rotation.angle() as f64);
		}
		
		for (&handle, node) in &self.collider_nodes {
			let pos = self.pipeline.collider(handle)
				.expect(&*format!("missing collider {:?}", handle)).position();
			let tr = pos.translation;
			let node = unsafe { node.assume_safe() };
			node.set_global_position(Vector2::new(tr.x as f32, tr.y as f32));
			node.set_global_rotation(pos.rotation.angle() as f64);
		}
	}
	
	fn sync_gd_xforms_to_physics(&mut self) {
		for &handle in &self.bodies_to_sync_to_physics {
			let body = self.pipeline.body_mut(handle)
				.expect(&*format!("missing body {:?}", handle));
			let node = unsafe { self.body_nodes[&handle].assume_safe() };
			let pos = node.global_position();
			let rot = node.global_rotation();
			let iso = Isometry::new(Vector::new(pos.x, pos.y), rot as Real);
			body.set_position(iso, false);
		}
	}
}

#[derive(NativeClass)]
#[inherit(Node)]
#[register_with(Self::_bind_methods)]
#[user_data(ArcData<Self>)]
pub struct R2dServer {
	inner: &'static AtomicRefCell<R2dServerInner>,
}

static SINGLETON: Lazy<AtomicRefCell<R2dServerInner>> = Lazy::new(|| AtomicRefCell::new(R2dServerInner::default()));

impl R2dServer {
	/// Signal emitted immediately before calling `PhysicsPipeline::step`
	pub const PRE_STEP: &'static str = "pre_step";
	/// Signal emitted immediately after `PhysicsPipeline::step` returns
	pub const STEPPED: &'static str = "stepped";
	
	pub fn singleton() -> Self {
		Self {
			inner: &*SINGLETON,
		}
	}
	
	fn _bind_methods(builder: &ClassBuilder<R2dServer>) {
		builder.add_signal(
			Signal {
				name: Self::STEPPED,
				args: &[]
			}
		);
		builder.add_signal(
			Signal {
				name: Self::PRE_STEP,
				args: &[]
			}
		);
	}
	
	pub(crate) fn insert_rigid_body(&self, body: &mut R2dRigidBody, owner: TRef<RigidBodyBase, Shared>) {
		let mut inner = self.inner.borrow_mut();
		
		let handle = inner.pipeline
			.insert_body(body.make_builder().build());
		inner.body_nodes.insert(handle, owner.claim());
		body.handle = handle;
		if body.sync_to_physics {
			inner.bodies_to_sync_to_physics.insert(handle);
		}
	}
	
	pub(crate) fn insert_collider(&self, collider: &mut R2dCollider, owner: TRef<<R2dCollider as NativeClass>::Base, Shared>) {
		let builder = collider.make_builder(&owner);
		
		let parent_handle = unsafe { R2dCollider::parent_handle(&*owner) };
		
		let mut inner = self.inner.borrow_mut();
		
		let handle = if let Some(parent) = parent_handle {
			let collider = builder.position(owner.local_isometry()).build();
			inner.pipeline.insert_collider_with_parent(collider, parent)
		} else {
			let collider = builder.position(owner.global_isometry()).build();
			inner.pipeline.insert_collider(collider)
		};
		
		inner.collider_nodes.insert(handle, owner.claim());
		collider.handle = handle;
	}
	
	pub(crate) fn insert_joint(&self, joint: &mut R2dJoint, owner: TRef<<R2dJoint as NativeClass>::Base, Shared>) {
		let params = match joint.make_params() {
			Some(builder) => builder,
			None => return,
		};
		
		let b1 = unsafe { joint.handle_1(&*owner) };
		let b2 = unsafe { joint.handle_2(&*owner) };
		
		let mut inner = SINGLETON.borrow_mut();
		
		let handle = inner.pipeline
			.insert_joint(params, b1, b2);
		
		inner.joint_nodes.insert(handle, owner.claim());
		joint.handle = handle;
	}
	
	pub(crate) fn remove_body(&self, body: &mut R2dRigidBody, _owner: TRef<RigidBodyBase, Shared>) {
		let handle = body.handle;
		let mut inner = self.inner.borrow_mut();
		inner.body_nodes.remove(&handle);
		inner.pipeline.remove_body(handle);
	}
	
	pub(crate) fn remove_collider(&self, collider: &mut R2dCollider, _owner: TRef<<R2dCollider as NativeClass>::Base, Shared>) {
		let (handle, wake_up) = (collider.handle, collider.wake_parent_on_remove);
		let mut inner = self.inner.borrow_mut();
		inner.collider_nodes.remove(&handle);
		inner.pipeline.remove_collider(handle, wake_up);
	}
	
	pub(crate) fn remove_joint(&self, joint: &mut R2dJoint, _owner: TRef<<R2dJoint as NativeClass>::Base, Shared>) {
		let (handle, wake_up) = (joint.handle, joint.wake_bodies_on_remove);
		let mut inner = self.inner.borrow_mut();
		inner.joint_nodes.remove(&handle);
		inner.pipeline.remove_joint(handle, wake_up);
	}
	
	pub fn mut_bodies<Ret>(&self, f: impl FnOnce(&mut RigidBodySet) -> Ret) -> Ret {
		f(self.inner.borrow_mut().pipeline.bodies_mut())
	}
	
	pub fn mut_colliders<Ret>(&self, f: impl FnOnce(&mut ColliderSet) -> Ret) -> Ret {
		f(self.inner.borrow_mut().pipeline.colliders_mut())
	}
}

#[methods]
impl R2dServer {
	fn new(_owner: &Node) -> Self {
		Self::singleton()
	}
	
	#[export]
	fn _enter_tree(&self, owner: &Node) {
		owner.set_physics_process(!Engine::godot_singleton().is_editor_hint())
	}
	
	#[export]
	fn _physics_process(&self, owner: &Node, dt: f64) {
		let mut inner = self.inner.borrow_mut();
		inner.pipeline.step_args.world.integration_parameters.dt = dt as Real;
		inner.sync_gd_xforms_to_physics();
		inner.step(owner);
		inner.update_godot_xforms();
	}
}
