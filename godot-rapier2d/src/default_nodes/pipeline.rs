use std::fmt::{Debug, Formatter};

use gdnative::{
	prelude::*,
	api::Resource,
	nativescript::{
		user_data::Once,
	},
};
use rapier2d::prelude::*;

#[cfg_attr(feature = "serde-serialize", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone)]
pub struct R2dWorld {
	pub gravity: Vector<Real>,
	pub integration_parameters: IntegrationParameters,
	pub islands: IslandManager,
	pub broad_phase: BroadPhase,
	pub narrow_phase: NarrowPhase,
	pub bodies: RigidBodySet,
	pub colliders: ColliderSet,
	pub joints: JointSet,
}

impl R2dWorld {
	fn new() -> Self {
		Self {
			gravity: Vector::<Real>::default(),
			integration_parameters: IntegrationParameters::default(),
			islands: IslandManager::new(),
			broad_phase: BroadPhase::new(),
			narrow_phase: NarrowPhase::new(),
			bodies: RigidBodySet::new(),
			colliders: ColliderSet::new(),
			joints: JointSet::new(),
		}
	}
	
	
}

impl Default for R2dWorld {
	fn default() -> Self {
		Self::new()
	}
}

impl Debug for R2dWorld {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		f.write_fmt(format_args!("R2dWorld {{ /* fields omitted */ }}"))
	}
}

pub struct R2dPipelineStepArgs<Bodies, Colliders> {
	pub world: R2dWorld,
	pub ccd_solver: CCDSolver,
	pub hooks: Box<dyn PhysicsHooks<Bodies, Colliders>>,
	pub events: Box<dyn EventHandler>,
}

impl<Bodies, Colliders> R2dPipelineStepArgs<Bodies, Colliders> {
	pub fn new() -> Self {
		Self {
			world: R2dWorld::default(),
			ccd_solver: CCDSolver::new(),
			hooks: Box::new(()),
			events: Box::new(()),
		}
	}
}

impl R2dPipelineStepArgs<RigidBodySet, ColliderSet> {
	#[inline(always)]
	pub fn step(&mut self, pipeline: &mut PhysicsPipeline) {
		let R2dPipelineStepArgs {
			world: R2dWorld {
				gravity,
				integration_parameters,
				islands,
				broad_phase,
				narrow_phase,
				bodies,
				colliders,
				joints,
			},
			ccd_solver,
			hooks,
			events,
		} = self;
		
		pipeline.step(
			gravity,
			integration_parameters,
			islands,
			broad_phase,
			narrow_phase,
			bodies,
			colliders,
			joints,
			ccd_solver,
			&**hooks,
			&**events,
		)
	}
}

impl<Bodies, Colliders> Default for R2dPipelineStepArgs<Bodies, Colliders> {
	fn default() -> Self {
		Self::new()
	}
}

#[derive(NativeClass, Default)]
#[inherit(Resource)]
#[user_data(Once<R2dPipeline>)]
pub struct R2dPipeline {
	pub pipeline: PhysicsPipeline,
	pub step_args: R2dPipelineStepArgs<RigidBodySet, ColliderSet>,
}

#[allow(dead_code)]
fn check_pipeline_send_sync() {
	fn do_test<T: Sync>() {}
	do_test::<R2dPipeline>();
}

#[methods]
impl R2dPipeline {
	fn new(_owner: &Resource) -> Self {
		Self::default()
	}
	
	#[inline(always)]
	pub fn step(&mut self) {
		self.step_args.step(&mut self.pipeline);
	}
	
	pub fn insert_body(&mut self, body: RigidBody) -> RigidBodyHandle {
		self.step_args.world.bodies.insert(body)
	}
	
	pub fn insert_collider(&mut self, collider: Collider) -> ColliderHandle {
		self.colliders_mut().insert(collider)
	}
	
	pub fn insert_collider_with_parent(&mut self, collider: Collider, parent: RigidBodyHandle) -> ColliderHandle {
		let R2dWorld {
			bodies,
			colliders,
			..
		} = self.world_mut();
		colliders.insert_with_parent(collider, parent, bodies)
	}
	
	pub fn insert_joint(
		&mut self,
		joint: JointParams,
		body1: RigidBodyHandle,
		body2: RigidBodyHandle,
	) -> JointHandle {
		let R2dWorld { bodies, joints, .. } = self.world_mut();
		joints.insert(bodies, body1, body2, joint)
	}
	
	pub fn remove_body(&mut self, handle: RigidBodyHandle) -> Option<RigidBody> {
		let R2dWorld {
			islands,
			bodies,
			colliders,
			joints,
			..
		} = self.world_mut();
		
		bodies.remove(handle, islands, colliders, joints)
	}
	
	pub fn remove_collider(&mut self, handle: ColliderHandle, wake_up: bool) -> Option<Collider> {
		let R2dWorld { islands, bodies, colliders, .. } = self.world_mut();
		colliders.remove(handle, islands, bodies, wake_up)
	}
	
	pub fn remove_joint(&mut self, handle: JointHandle, wake_up: bool) -> Option<Joint> {
		let R2dWorld { islands, bodies, joints, ..} = self.world_mut();
		joints.remove(handle, islands, bodies, wake_up)
	}
	
	pub fn bodies(&self) -> &RigidBodySet {
		&self.step_args.world.bodies
	}
	
	pub fn colliders(&self) -> &ColliderSet {
		&self.step_args.world.colliders
	}
	
	pub fn joints(&self) -> &JointSet {
		&self.step_args.world.joints
	}
	
	pub fn world(&self) -> &R2dWorld {
		&self.step_args.world
	}
	
	pub fn bodies_mut(&mut self) -> &mut RigidBodySet {
		&mut self.step_args.world.bodies
	}
	
	pub fn colliders_mut(&mut self) -> &mut ColliderSet {
		&mut self.step_args.world.colliders
	}
	
	pub fn joints_mut(&mut self) -> &mut JointSet {
		&mut self.step_args.world.joints
	}
	
	pub fn world_mut(&mut self) -> &mut R2dWorld {
		&mut self.step_args.world
	}
	
	pub fn body(&self, handle: RigidBodyHandle) -> Option<&RigidBody> {
		self.bodies().get(handle)
	}
	
	pub fn collider(&self, handle: ColliderHandle) -> Option<&Collider> {
		self.colliders().get(handle)
	}
	
	pub fn joint(&self, handle: JointHandle) -> Option<&Joint> {
		self.joints().get(handle)
	}
	
	pub fn body_mut(&mut self, handle: RigidBodyHandle) -> Option<&mut RigidBody> {
		self.bodies_mut().get_mut(handle)
	}
	
	pub fn collider_mut(&mut self, handle: ColliderHandle) -> Option<&mut Collider> {
		self.colliders_mut().get_mut(handle)
	}
	
	pub fn joint_mut(&mut self, handle: JointHandle) -> Option<&mut Joint> {
		self.joints_mut().get_mut(handle)
	}
	
}