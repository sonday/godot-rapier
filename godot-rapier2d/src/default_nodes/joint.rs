use gdnative::{
	api::{Engine, NativeScript, Resource},
	nativescript::{Map, MapMut, },
	prelude::*,
};
use rapier2d::prelude::*;

use crate::{
	default_nodes::{
		rigid_body::R2dRigidBody,
		server::R2dServer,
	},
	dynamics::joint_params::*,
};
use crate::default_nodes::R2dDeadlockPolicy;

#[derive(NativeClass)]
#[inherit(Node2D)]
#[register_with(Self::bind_methods::<Self>)]
#[user_data(RwLockData<Self, R2dDeadlockPolicy>)]
pub struct R2dJoint {
	pub joint_params: Option<Ref<Resource, Shared>>,
	pub body_1: NodePath,
	pub body_2: NodePath,
	pub wake_bodies_on_remove: bool,
	pub handle: JointHandle,
}

impl Clone for R2dJoint {
	fn clone(&self) -> Self {
		Self {
			joint_params: self.joint_params.clone(),
			body_1: self.body_1.new_ref(),
			body_2: self.body_2.new_ref(),
			wake_bodies_on_remove: self.wake_bodies_on_remove,
			handle: self.handle,
		}
	}
}

impl AsRef<R2dJoint> for R2dJoint {
	#[inline]
	fn as_ref(&self) -> &Self {
		self
	}
}

impl AsMut<R2dJoint> for R2dJoint {
	#[inline]
	fn as_mut(&mut self) -> &mut Self {
		self
	}
}

#[methods]
impl R2dJoint {
	fn new(_owner: &Node2D) -> Self {
		Self {
			joint_params: None,
			body_1: NodePath::default(),
			body_2: NodePath::default(),
			wake_bodies_on_remove: true,
			handle: JointHandle::invalid(),
		}
	}
	
	pub fn bind_methods<C: NativeClass + AsRef<Self> + AsMut<Self>>(builder: &ClassBuilder<C>)
	where <C as NativeClass>::UserData: Map + MapMut,
	      <C as NativeClass>::Base: SubClass<<Self as NativeClass>::Base> {
		builder.add_property::<NodePath>("body_1")
			.with_default(NodePath::default())
			.with_getter(|this, _| this.as_ref().body_1.new_ref())
			.with_setter(|this, _, value| { this.as_mut().body_1 = value })
			.done();
		builder.add_property::<NodePath>("body_2")
			.with_default(NodePath::default())
			.with_getter(|this, _| this.as_ref().body_2.new_ref())
			.with_setter(|this, _, value| { this.as_mut().body_2 = value })
			.done();
		builder.add_property::<bool>("wake_bodies_on_remove")
			.with_default(false)
			.with_getter(|this, _| this.as_ref().wake_bodies_on_remove)
			.with_setter(|this, _, value| { this.as_mut().wake_bodies_on_remove = value })
			.done()
	}
	
	#[export]
	fn _get_configuration_warning(&self, owner: &Node2D) -> &'static str {
		let body_1 = owner.get_node(self.body_1.new_ref());
		if body_1.is_none() { return "Couldn't find body_1" };
		let body_1 = unsafe { body_1.unwrap().assume_safe() }.cast::<Node2D>()
			.and_then(|body| body.cast_instance::<R2dRigidBody>());
		if body_1.is_none() {
			return "body_1 should refer to an R2dRigidBody"
		}
		
		let body_2 = owner.get_node(self.body_2.new_ref());
		if body_2.is_none() { return "Couldn't find body_2" };
		let body_2 = unsafe { body_2.unwrap().assume_safe() }.cast::<Node2D>()
			.and_then(|body| body.cast_instance::<R2dRigidBody>());
		
		if body_2.is_none() {
			return "body_2 should refer to an R2dRigidBody"
		}
		""
	}
	
	#[inline]
	pub unsafe fn handle_1(&self, owner: &<Self as NativeClass>::Base) -> RigidBodyHandle {
		Self::handle(owner, &self.body_2)
	}
	
	#[inline]
	pub unsafe fn handle_2(&self, owner: &<Self as NativeClass>::Base) -> RigidBodyHandle {
		Self::handle(owner, &self.body_1)
	}
	
	#[inline]
	unsafe fn handle(owner: &<Self as NativeClass>::Base, path: &NodePath) -> RigidBodyHandle {
		owner
			.get_node(path.new_ref())
			.and_then(|node| {
				let node = node.assume_safe();
				node
					.cast::<Node2D>()
					.and_then(TRef::cast_instance::<R2dRigidBody>)
					.map_or_else(
						|| {
							godot_error!("failed to get RigidBodyHandle from {:?}", path);
							None
						},
						|inst| inst.script().map(|body| body.handle)
							.map_err(|e| {
								godot_error!("failed to map R2dRigidBody instance");
								e
							}).ok()
					)
			})
			.unwrap_or(RigidBodyHandle::invalid())
	}
	
	pub fn make_params(&self) -> Option<JointParams> {
		self.joint_params.as_ref()
			.or_else(|| { godot_warn!("missing JointParams"); None })
			.and_then(|res: &Ref<Resource, Shared>| {
				let res = unsafe { res.assume_safe() };
				res.get_script()
					.and_then(|script| script.cast::<NativeScript>())
					.or_else(|| { godot_warn!("resource is not an instance of JointParams"); None })
					.and_then(|script| {
						let class_name = unsafe { script.assume_safe() }.class_name().to_string();
						
						use gdnative::nativescript::Map;
						macro_rules! joint_params {
							($t:ty) => {{
								res.cast_instance::<$t>()
									.and_then(|instance| instance
										.script()
										.map(|joint| joint.params())
										.ok()
										.or_else(|| { godot_warn!("failed to map {:?} script", class_name); None })
									)
									.or_else(|| { godot_warn!("failed to cast {:?} instance", class_name); None })
							}}
						}
						
						if R2dBallJoint::class_name() == class_name { joint_params!(R2dBallJoint) }
						else if R2dFixedJoint::class_name() == class_name { joint_params!(R2dFixedJoint) }
						else if R2dPrismaticJoint::class_name() == class_name { joint_params!(R2dPrismaticJoint) }
						else { godot_warn!("{:?} is not a type of JointParams", class_name); None }
					})
			})
	}
	
	#[export]
	fn _notification(&mut self, owner: TRef<Node2D, Shared>, what: i64) {
		if what == Node::NOTIFICATION_POST_ENTER_TREE // children will be ready here, though lower siblings may still not be?
			&& !Engine::godot_singleton().is_editor_hint()
		{
			R2dServer::singleton().insert_joint(self, owner);
		}
	}
	
	#[export]
	fn _exit_tree(&mut self, owner: TRef<Node2D, Shared>) {
		if !Engine::godot_singleton().is_editor_hint() {
			R2dServer::singleton().remove_joint(self, owner);
		}
		self.handle = JointHandle::invalid();
	}
}

