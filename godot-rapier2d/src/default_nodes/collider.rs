use gdnative::{
	api::Engine,
	nativescript::Map,
	prelude::*,
};
use rapier2d::{
	data::ComponentSetMut,
	prelude::*,
};

use crate::{
	default_nodes::{
		rigid_body::R2dRigidBody,
		server::R2dServer,
	},
	geometry::{
		collider::{ColliderComponentAccessors, ColliderComponents, ColliderProperties, ColliderResourceAccessors, },
		shapes::{Draw, PlaceholderShape},
	},
	traits::R2dNode,
	utils::{GetIsometry, ToRapier},
};
use crate::default_nodes::R2dDeadlockPolicy;
use crate::traits::MapStorageMut;

#[derive(NativeClass)]
#[inherit(Node2D)]
#[register_with(Self::register_r2d_properties)]
#[user_data(RwLockData<Self, R2dDeadlockPolicy>)]
pub struct R2dCollider {
	components: ColliderComponents,
	pub handle: ColliderHandle,
	#[property(default = false)]
	pub wake_parent_on_remove: bool,
	pub should_draw: bool,
	pub user_data: u128,
	in_tree: bool,
}

impl R2dCollider {
	fn new(_owner: &Node2D) -> Self {
		Self {
			components: ColliderComponents::new(),
			handle: ColliderHandle::invalid(),
			wake_parent_on_remove: false,
			should_draw: Engine::godot_singleton().is_editor_hint(),
			user_data: 0,
			in_tree: false,
		}
	}
	
	pub fn make_builder(&self, owner: &Node2D) -> rapier2d::geometry::ColliderBuilder {
		let shape = self.shape().as_ref()
			.map_or(
				ColliderShape::new(PlaceholderShape),
				|inst| inst.shape.shared_shape()
			);
		
		let material: ColliderMaterial = self.material().to_rapier();
		let flags = self.flags();
		
		ColliderBuilder::new(shape)
			.position(owner.global_isometry())
			// .density(owner.density()) //TODO: Collider mass_props property
			.friction(material.friction)
			.friction_combine_rule(material.friction_combine_rule)
			.restitution(material.restitution)
			.restitution_combine_rule(material.restitution_combine_rule)
			.sensor(self.collider_type() == &ColliderType::Sensor)
			.collision_groups(flags.collision_groups)
			.solver_groups(flags.solver_groups)
			.active_hooks(self.active_hooks().to_rapier())
			.active_events(self.active_events().to_rapier())
			.active_collision_types(self.active_collision_types().to_rapier())
			.user_data(self.user_data)
	}
	
	pub unsafe fn parent_handle(owner: &Node2D) -> Option<RigidBodyHandle> {
		owner
			.get_parent()
			.or_else(|| {
				godot_warn!("missing parent");
				None
			})
			.and_then(|parent| parent.assume_safe()
				.cast::<Node2D>()
				.or_else(|| {
					godot_warn!("failed to cast parent");
					None
				})
			)
			.and_then(|node| node.cast_instance::<R2dRigidBody>()
				.or_else(|| {
					godot_warn!("failed to cast parent to R2dRigidBody");
					None
				})
			)
			.and_then(|inst| inst.script().map(|body| body.handle)
				.map_err(|e| {
					godot_warn!("failed to map R2dRigidBody instance: {:?}", e);
					e
				}).ok()
			)
	}
}

#[methods]
impl R2dCollider {
	#[export]
	fn _enter_tree(&mut self, owner: TRef<Node2D, Shared>) {
		self.set_position(ColliderPosition(owner.global_isometry()));
		
		if !Engine::godot_singleton().is_editor_hint() {
			owner.set_process_internal(true);
			
			self.should_draw = unsafe { owner.get_tree().unwrap().assume_safe() }
				.is_debugging_collisions_hint();
			
			R2dServer::singleton().insert_collider(self, owner);
			self.in_tree = true;
		}
	}
	
	#[export]
	fn _exit_tree(&mut self, owner: TRef<Node2D, Shared>) {
		self.in_tree = false;
		owner.set_process_internal(false);
		if !Engine::godot_singleton().is_editor_hint() {
			R2dServer::singleton().remove_collider(self, owner);
		}
		self.handle = ColliderHandle::invalid();
	}
	
	#[export]
	fn _draw(&mut self, owner: &Node2D) {
		if self.should_draw {
			self.shape_instance().as_ref().map(|shape| shape.draw(owner.upcast()));
		}
	}
	
	#[export]
	fn _shape_changed(&self, owner: &Node2D) {
		owner.update();
	}
	
	#[export] // Overriding this requires a Godot modification -- may want to PR
	fn _edit_is_selected_on_click(&self, _owner: &Node2D, point: Vector2, tolerance: f64) -> bool {
		self.shape().as_ref()
		    .map(|it| it.shape.is_selected_on_click(point, tolerance))
		    .unwrap_or_else(|| point.length() as f64 <= tolerance)
	}
	
	#[export]
	fn _get_configuration_warning(&self, _owner: &Node2D) -> &'static str {
		match self.shape_instance() {
			&None => "Shape property is not set",
			_ => "",
		}
	}
}

impl AsRef<ColliderComponents> for R2dCollider {
	#[inline]
	fn as_ref(&self) -> &ColliderComponents {
		&self.components
	}
}

impl AsMut<ColliderComponents> for R2dCollider {
	#[inline]
	fn as_mut(&mut self) -> &mut ColliderComponents {
		&mut self.components
	}
}

impl R2dNode<ColliderComponents> for R2dCollider {
	#[inline]
	fn components(&self) -> &ColliderComponents {
		self.as_ref()
	}
	
	#[inline]
	fn components_mut(&mut self) -> &mut ColliderComponents {
		self.as_mut()
	}
}

impl<C, Ret> MapStorageMut<C, Ret> for R2dCollider
where ColliderSet: ComponentSetMut<C> {
	#[inline]
	fn map_storage_mut(&mut self, f: impl for<'c> FnOnce(&'c mut C) -> Ret) -> () {
		if self.in_tree {
			R2dServer::singleton()
				.mut_colliders(|colliders| colliders.map_mut_internal(self.handle.0, f))
				.or_else(|| { godot_error!("failed to set component {}", std::any::type_name::<C>()); None });
		}
	}
}