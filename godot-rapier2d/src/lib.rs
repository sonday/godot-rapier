pub extern crate rapier2d;

#[doc(inline)]
pub use dynamics::rigid_body::{RigidBodyComponents, RigidBodyProperties};
use gdnative::{
	prelude::*,
};
#[doc(inline)]
pub use geometry::collider::{ColliderComponents, ColliderProperties};
#[doc(inline)]
pub use traits::R2dNode;

#[macro_use]
pub(crate) mod macros;

#[cfg(feature = "default_nodes")]
pub mod default_nodes;
pub mod utils;
pub mod dynamics;
pub mod geometry;
pub mod traits;

pub mod prelude {
	#[cfg(feature = "default_nodes")]
	pub use crate::default_nodes::*;
	pub use crate::{
		dynamics::{joint_params::*, rigid_body::*,},
		geometry::{collider::*, material::*, shapes::*, },
		traits::{R2dNode, MapStorageMut,},
		utils::*,
	};
}

/// Call this function in your [godot_init!(init)](::gdnative::prelude::godot_init) callback.
pub fn init(handle: InitHandle) {
	
	use dynamics::joint_params::*;
	handle.add_tool_class::<R2dBallJoint>();
	handle.add_tool_class::<R2dFixedJoint>();
	handle.add_tool_class::<R2dPrismaticJoint>();
	
	use geometry::*;
	handle.add_tool_class::<material::R2dColliderMaterial>();
	
	use geometry::shapes::*;
	handle.add_tool_class::<R2dBall>();
	handle.add_tool_class::<R2dSegment>();
	handle.add_tool_class::<R2dCapsuleX>();
	handle.add_tool_class::<R2dCapsuleY>();
	handle.add_tool_class::<R2dCuboid>();
	handle.add_tool_class::<R2dRoundCuboid>();
	handle.add_tool_class::<R2dTriangle>();
	handle.add_tool_class::<R2dHalfSpace>();
	
	#[cfg(feature = "default_nodes")]
	init_default_nodes(handle);
}

#[cfg(feature = "default_nodes")]
fn init_default_nodes(handle: InitHandle) {
	use default_nodes::*;
	handle.add_tool_class::<R2dServer>();
	handle.add_tool_class::<R2dPipeline>();
	handle.add_tool_class::<R2dCollider>();
	handle.add_tool_class::<R2dRigidBody>();
	handle.add_tool_class::<R2dJoint>();
}