use gdnative::prelude::*;

fn init(handle: InitHandle) {
	godot_rapier2d::init(handle);
	handle.add_class::<R2dPlugin>();
}

godot_init!(init);

use gdnative::api::{EditorPlugin, Texture, Script,};

const ADDON_PATH: &str = "res://addons/rapier2d";

#[derive(NativeClass, Default)]
#[inherit(EditorPlugin)]
pub struct R2dPlugin {
	types: Vec<&'static str>,
}

#[methods]
impl R2dPlugin {
	fn new(_owner: TRef<EditorPlugin>) -> Self {
		Self::default()
	}
	
	#[export]
	fn _enter_tree(&mut self, owner: TRef<EditorPlugin>) {
		
		macro_rules! add_type {
			($t:ty, $inherits:ty, $script:literal, $icon:literal) => {
				let loader = ResourceLoader::godot_singleton();
				let script = unsafe {
					let resource = loader.load(ADDON_PATH.to_owned() + concat!("/scripts/", $script, ".gdns"), "Script", false).expect(concat!("load ", $script));
					let resource = resource.assume_safe().claim();
					resource.cast::<Script>().expect(concat!("cast ", $script, ".gdns to Script"))
				};
				
				let texture = unsafe {
					let tex = loader.load(ADDON_PATH.to_owned() + concat!("/icons/", $icon), "Texture", false).expect(concat!("load ", $icon));
					let tex = tex.assume_safe().claim();
					tex.cast::<Texture>().expect(concat!("cast ", $icon, " to Texture"))
				};
				
				let t = stringify!($t);
				owner.add_custom_type(t, stringify!($inherits), script, texture);
				self.types.push(t);
			};
			($t:ty, $inherits:ty, $script:literal) => {
				let loader = ResourceLoader::godot_singleton();
				let script = unsafe {
					let resource = loader.load(ADDON_PATH.to_owned() + concat!("/scripts/", $script, ".gdns"), "Script", false).expect(concat!("load ", $script));
					let resource = resource.assume_safe().claim();
					resource.cast::<Script>().expect(concat!("cast ", $script, ".gdns to Script"))
				};
				
				let texture = Texture::null();
				
				let t = stringify!($t);
				owner.add_custom_type(t, stringify!($inherits), script, texture);
				self.types.push(t);
			}
		}
		
		add_type!(R2dBody, Node2D, "r2d_body", "feature_contact_models.png");
		
		add_type!(R2dJoint, Node2D, "r2d_joint", "feature_contact_models.png");
		add_type!(R2dBallJoint, Node2D, "r2d_ball_joint", "feature_contact_models.png");
		add_type!(R2dFixedJoint, Node2D, "r2d_fixed_joint", "feature_contact_models.png");
		add_type!(R2dPrismaticJoint, Node2D, "r2d_prismatic_joint", "feature_contact_models.png");
		
		add_type!(R2dCollider, Node2D, "r2d_collider", "feature_contact_models.png");
		
		add_type!(R2dBall, Resource, "r2d_ball", "feature_contact_models.png");
		add_type!(R2dSegment, Resource, "r2d_segment", "feature_contact_models.png");
		add_type!(R2dCapsuleX, Resource, "r2d_capsule_x", "feature_contact_models.png");
		add_type!(R2dCapsuleY, Resource, "r2d_capsule_y", "feature_contact_models.png");
		add_type!(R2dCuboid, Resource, "r2d_cuboid", "feature_contact_models.png");
		add_type!(R2dRoundCuboid, Resource, "r2d_round_cuboid", "feature_contact_models.png");
		// add_type!(R2dHalfSpace, Resource, "r2d_half_space", "feature_contact_models.png");
		
		add_type!(R2dPipeline, Resource, "r2d_pipeline", "feature_contact_models.png");
		add_type!(R2dEngine, Node2D, "r2d_engine", "feature_contact_models.png");
	}
	
	#[export]
	fn _exit_tree(&mut self, owner: TRef<EditorPlugin>) {
		for t in self.types.drain(..) {
			owner.remove_custom_type(t)
		}
	}
}
