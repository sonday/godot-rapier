use gdnative_project_utils::*;
use std::path::Path;

const ADDON_DIR: &'static str = "addon_dir";

fn main() -> Result<(), Box<dyn std::error::Error>> {
	let classes = scan_crate("../godot-rapier2d/src")?;
	
	let addon_path = Path::new(ADDON_DIR);
	
	let gen = Generator::new()
		.godot_project_dir(addon_path)
		.build(r2d_classes)?;
	
	Ok(())
}